﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvertMaskImg : MonoBehaviour {
	Image img;
	// Use this for initialization
	void Awake () {
		img = GetComponent<Image> ();
		Material mat = img.materialForRendering;
		img.RegisterDirtyMaterialCallback (OnMaterialDirty);
	}

	void OnMaterialDirty()
	{
		Material mat = img.materialForRendering;
		mat.SetInt ("_StencilComp", 6);
	}
		
	void OnDesotry()
	{
		if (img != null) 
		{
			img.UnregisterDirtyMaterialCallback (OnMaterialDirty);
		}
	}
}
