﻿using System;
using System.Text.RegularExpressions;

public static class RegexUtil {
    /// <summary>
    /// 屏蔽emoji 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string filtrationEmoji (string input) {
        string result = Regex.Replace(input, @"\p{Cs}", "");
        return result;
    }

    /// <summary>
    /// 判断输入的字符串是否是一个合法的手机号
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static bool IsMobilePhone (string input) {
        return IsMatch(@"^1[\d]{10}$", input.Trim());
        //return IsMatch(@"^(13\d|14[57]|15[^4,\D]|17[678]|18\d)\d{8}|170[059]\d{7}$", input.Trim());
    }

    /// <summary>
    /// 判断昵称字符串是否合法
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static bool IsUserName (string input) {
        return IsMatch(@"^[A-Za-z0-9_\u4e00-\u9fa5]{1,20}$", input.Trim());
    }

    /// <summary>
    /// 校验密码：只能输入6-20个字母、数字、下划线
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static bool IsPassword (string input) {
        return IsMatch(@"^(\w){6,20}$", input);
    }

    #region 正则的通用方法
    /// <summary>
    /// 计算字符串的字符长度，一个汉字字符将被计算为两个字符
    /// </summary>
    /// <param name="input">需要计算的字符串</param>
    /// <returns>返回字符串的长度</returns>
    public static int GetCount (string input) {
        return Regex.Replace(input, @"[\u4e00-\u9fa5/g]", "aa").Length;
    }

    /// <summary>
    /// 调用Regex中IsMatch函数实现一般的正则表达式匹配
    /// </summary>
    /// <param name="pattern">要匹配的正则表达式模式。</param>
    /// <param name="input">要搜索匹配项的字符串</param>
    /// <returns>如果正则表达式找到匹配项，则为 true；否则，为 false。</returns>
    public static bool IsMatch (string pattern, string input) {
        if (input == null || input == "") return false;
        Regex regex = new Regex(pattern);
        return regex.IsMatch(input);
    }

    /// <summary>
    /// 从输入字符串中的第一个字符开始，用替换字符串替换指定的正则表达式模式的所有匹配项。
    /// </summary>
    /// <param name="pattern">模式字符串</param>
    /// <param name="input">输入字符串</param>
    /// <param name="replacement">用于替换的字符串</param>
    /// <returns>返回被替换后的结果</returns>
    public static string Replace (string pattern, string input, string replacement) {
        Regex regex = new Regex(pattern);
        return regex.Replace(input, replacement);
    }

    /// <summary>
    /// 在由正则表达式模式定义的位置拆分输入字符串。
    /// </summary>
    /// <param name="pattern">模式字符串</param>
    /// <param name="input">输入字符串</param>
    /// <returns></returns>
    public static string[] Split (string pattern, string input) {
        Regex regex = new Regex(pattern);
        return regex.Split(input);
    }

    /* *******************************************************************
     * 1、通过“:”来分割字符串看得到的字符串数组长度是否小于等于8
     * 2、判断输入的IPV6字符串中是否有“::”。
     * 3、如果没有“::”采用 ^([\da-f]{1,4}:){7}[\da-f]{1,4}$ 来判断
     * 4、如果有“::” ，判断"::"是否止出现一次
     * 5、如果出现一次以上 返回false
     * 6、^([\da-f]{1,4}:){0,5}::([\da-f]{1,4}:){0,5}[\da-f]{1,4}$
     * ******************************************************************/
    /// <summary>
    /// 判断字符串compare 在 input字符串中出现的次数
    /// </summary>
    /// <param name="input">源字符串</param>
    /// <param name="compare">用于比较的字符串</param>
    /// <returns>字符串compare 在 input字符串中出现的次数</returns>
    private static int GetStringCount (string input, string compare) {
        int index = input.IndexOf(compare);
        if (index != -1) {
            return 1 + GetStringCount(input.Substring(index + compare.Length), compare);
        } else {
            return 0;
        }

    }
    ///// <summary>
    ///// 根据结构体RegularFormula,返回具体正则表达式
    ///// </summary>
    ///// <param name="RegulatFormula"></param>
    ///// <returns></returns>
    //private static string GetRegularFormula(string RegulatFormula)
    //{
    //    string str = "";
    //    if (RegulatFormula.ToUpper().IndexOf("RegularFormula".ToUpper()) > -1)
    //    {
    //        RegularFormula formula = new RegularFormula();
    //        Type type = typeof(RegularFormula);
    //        foreach (System.Reflection.FieldInfo info in type.GetFields())
    //        {
    //            if (("RegularFormula." + info.Name).ToUpper() == RegulatFormula.ToUpper())
    //            {
    //                str = info.GetValue(formula).ToString();
    //            }
    //        }
    //        return str;
    //    }
    //    return RegulatFormula;
    //}

    #endregion
}