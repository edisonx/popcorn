﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

public class BuildEditor {
    static void StartBuildAndroid() {
        try {
            Debug.Log("Android打包开始!!!");
            PackageWindow.StartPackage();
            var allScene = EditorBuildSettings.scenes;
            List<EditorBuildSettingsScene> buildScenes = new List<EditorBuildSettingsScene>();
            for (int i = 0; i < allScene.Length; i++) {
                if (allScene[i].enabled) {
                    buildScenes.Add(allScene[i]);
                }
            }

            DeleteOtherPlatforStremAsset();
            DeleteManifest();

            string path = Application.dataPath + "/../AndroidProject";
            if (Directory.Exists(path))
                Directory.Delete(path, true);
            Directory.CreateDirectory(path);
            EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
            BuildPipeline.BuildPlayer(buildScenes.ToArray(), path, BuildTarget.Android, BuildOptions.AcceptExternalModificationsToPlayer);
            Debug.Log("Android 打包成功");
        } catch (Exception e) {
            Debug.Log("Android打包失败!!!" + e.Message);
        }
    }

    static void StartBuildIos() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.IOS;
        PackageWindow.StartPackage();
        var allScene = EditorBuildSettings.scenes;
        List<EditorBuildSettingsScene> buildScenes = new List<EditorBuildSettingsScene>();
        for (int i = 0; i < allScene.Length; i++) {
            if (allScene[i].enabled) {
                buildScenes.Add(allScene[i]);
            }
        }

        DeleteOtherPlatforStremAsset();
        DeleteManifest();

        string path = Application.dataPath + "/../IosProject";
        if (Directory.Exists(path))
            Directory.Delete(path, true);
        Directory.CreateDirectory(path);
        BuildPipeline.BuildPlayer(buildScenes.ToArray(), path, BuildTarget.iOS, BuildOptions.AcceptExternalModificationsToPlayer);
        Debug.Log("IOS 打包成功");
    }

    #region android build
    [MenuItem("Tools/Build/Android/OpenDebug")]
    public static void BuildAndroidDebug() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.Android;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(true, false);
        StartBuildAndroid();
    }

    [MenuItem("Tools/Build/Android/NoDebug")]
    public static void BuildAndroid() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.Android;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(false, false);
        StartBuildAndroid();
    }

    [MenuItem("Tools/Build/Android/TestAndDebug")]
    public static void BuildAndroidDebugTestEnv() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.Android;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(true, true);
        StartBuildAndroid();
    }

    [MenuItem("Tools/Build/Android/TestNoDebug")]
    public static void BuildAndroidTestEnv() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.Android;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(false, true);
        StartBuildAndroid();
    }

    #endregion

    #region Ios build
    [MenuItem("Tools/Build/Ios/OpenDebug")]
    public static void BuildIosDebug() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.IOS;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(true, false);
        StartBuildIos();
    }

    [MenuItem("Tools/Build/Ios/NoDebug")]
    public static void BuildIos() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.IOS;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(false, false);
        StartBuildIos();
    }

    [MenuItem("Tools/Build/Ios/TestAndDebug")]
    public static void BuildIosDebugTestEnv() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.IOS;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(true, true);
        StartBuildIos();
    }

    [MenuItem("Tools/Build/Ios/TestNoDebug")]
    public static void BuildIosTestEnv() {
        PackagePlatform.platformCurrent = PackagePlatform.PlatformType.IOS;
        RuntimeEnvEditor.SwitchCrtPackgePlatformSymbol(false, true);
        StartBuildIos();
    }

    #endregion

    static void DeleteOtherPlatforStremAsset() {
        var platforms = Enum.GetValues(typeof(PackagePlatform.PlatformType));
        string platDir = string.Empty;
        foreach (PackagePlatform.PlatformType p in platforms) {
            if (p != PackagePlatform.platformCurrent) //删除steamasset目录中的其他文件
            {
                platDir = Application.streamingAssetsPath + "/" + "Package_" + p.ToString() + "/";
                if (Directory.Exists(platDir))
                    Directory.Delete(platDir, true);
            }
        }

    }

    static void DeleteManifest() {
        string platDir = Application.streamingAssetsPath + "/" + "Package_" + PackagePlatform.platformCurrent.ToString() + "/";
        var files = Directory.GetFiles(platDir, "*.manifest", SearchOption.AllDirectories);
        for (int i = 0; i < files.Length; i++) {
            AssetDatabase.DeleteAsset(files[i].Replace(Application.dataPath, "Assets"));
        }
    }
}
