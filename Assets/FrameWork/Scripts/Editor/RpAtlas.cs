﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;


public class RpAtlas : EditorWindow
{
    [MenuItem("Tools/RpAtlas")]
    static public void OpenRpAtlas()
    {
        EditorWindow.GetWindow<RpAtlas>(false, "RpAtlas", true).Show();
    }
    public RpAtlas()
    {
    }
    [SerializeField]
    private GameObject targetGo;
    [SerializeField]
    private static Texture2D targetTexture;

    public static Dictionary<string, Object> dir;

    private static void SaveMessage()
    {
        Object obj = targetTexture as Object;
        string path = AssetDatabase.GetAssetPath(obj);
        var sprites = AssetDatabase.LoadAllAssetsAtPath(path);
        dir = new Dictionary<string, Object>();
        for (int i = 0; i < sprites.Length; i++)
        {
            if (dir.ContainsKey(sprites[i].name) == false)
            {
                dir.Add(sprites[i].name, sprites[i]);
            }
        }
    }

    void OnGUI()
    {
        targetGo = EditorGUILayout.ObjectField("Target GameObject", targetGo, typeof(GameObject), false) as GameObject;
        targetTexture = EditorGUILayout.ObjectField("Target Texture", targetTexture, typeof(Texture2D), false) as Texture2D;
        if (GUILayout.Button("Start RpImageAtlas"))
        {
            SaveMessage();
            FindChildImage(targetGo.transform);
            EditorUtility.SetDirty(targetGo);
        }
        if (GUILayout.Button("Start RpButtonAtlas"))
        {
            SaveMessage();
            FindChildButton(targetGo.transform);
            EditorUtility.SetDirty(targetGo);
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private static void FindChildButton(Transform targetGo)
    {
        Button btn = targetGo.GetComponent<Button>();
        SpriteState ss = new SpriteState();
        if (btn != null && btn.transition == Selectable.Transition.SpriteSwap)
        {
            string spriteName = string.Empty;
            ss = btn.spriteState;
            if (btn.spriteState.highlightedSprite != null)
            {
                spriteName = ss.highlightedSprite.name;
                if (dir.ContainsKey(spriteName))
                {
                    ss.highlightedSprite = dir[spriteName] as Sprite;
                    btn.spriteState = ss;
                }
                else
                {
                    Debug.Log(" Not found this HighlightedSprite :   " + spriteName + "  Path is  :  " + AssetDatabase.GetAssetPath(targetGo) + "   " + targetGo.name);
                }
            }
            if (btn.spriteState.pressedSprite != null)
            {
                spriteName = ss.pressedSprite.name;
                if (dir.ContainsKey(spriteName))
                {
                    ss.pressedSprite = dir[spriteName] as Sprite;
                    btn.spriteState = ss;
                }
                else
                {
                    Debug.Log(" Not found this PressedSprite :   " + spriteName + "  Path is  :  " + AssetDatabase.GetAssetPath(targetGo) + "   " + targetGo.name);
                }
            }
            if (btn.spriteState.disabledSprite != null)
            {
                spriteName = ss.disabledSprite.name;
                if (dir.ContainsKey(spriteName))
                {
                    ss.disabledSprite = dir[spriteName] as Sprite;
                    btn.spriteState = ss;
                }
                else
                {
                    Debug.Log(" Not found this DisabledSprite :   " + spriteName + "  Path is  :  " + AssetDatabase.GetAssetPath(targetGo) + "   " + targetGo.name);
                }
            }
        }
        foreach (Transform child in targetGo)
        {
            FindChildButton(child);
        }
    }
    private static void FindChildImage(Transform targetGo)
    {
        Image img = targetGo.GetComponent<Image>();
        if (img != null)
        {
            string spriteName = string.Empty;
            if (img.overrideSprite != null)
            {
                spriteName = img.overrideSprite.name;
                if (dir.ContainsKey(spriteName))
                {
                    img.sprite = dir[spriteName] as Sprite;
                }
                else
                {
                    Debug.Log(" Not found this image :   " + spriteName + "  Path is  :  " + AssetDatabase.GetAssetPath(targetGo) + "   " + targetGo.name);
                }
            }
        }
        foreach (Transform child in targetGo)
        {
            FindChildImage(child);
        }
    }
}