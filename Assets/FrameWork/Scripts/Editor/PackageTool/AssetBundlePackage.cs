﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AssetBundlePackage 
{
    public static void BuildAssetbundleWindows()
    {
        BuildAssetbundlesSpecify(PackagePlatform.PlatformType.Windows);
    }
    public static void BuildAssetbundleIOS()
    {
        BuildAssetbundlesSpecify(PackagePlatform.PlatformType.IOS);
    }
    public static void BuildAssetbundleAndroid()
    {
        BuildAssetbundlesSpecify(PackagePlatform.PlatformType.Android);
    }

    public static void BuildAssetbundlesSpecify(PackagePlatform.PlatformType rBuildPlatform)
    {

        PackagePlatform.platformCurrent = rBuildPlatform;

        List<AssetBundleBuild> abbList = GeneratorAssetbundleEntry();
        if (abbList == null)
        {
            Debug.Log("没有找到需要打包的资源");
            return;
        } 

        string rPath = PackagePlatform.GetAssetBundlesPath();
        if (Directory.Exists(rPath) == false)
            Directory.CreateDirectory(rPath);
        BuildPipeline.BuildAssetBundles(rPath, abbList.ToArray(), BuildAssetBundleOptions.None, PackagePlatform.GetBuildTarget());

        for (int i = 0; i < abbList.Count; i++)
        {
            PackageUtil.PushResABBuild(abbList[i]);
        }
    }

    static List<AssetBundleBuild> GeneratorAssetbundleEntry()
    {
       
        string path = Application.dataPath + "/" + PackagePlatform.packageConfigPath;
        if (string.IsNullOrEmpty(path)) return null;

        string str = File.ReadAllText(path);
        
        Dict<string, ABEntry> abEntries = new Dict<string, ABEntry>();

        PackageConfig apc = JsonUtility.FromJson<PackageConfig>(str);

        AssetBundlePackageInfo[] bundlesInfo = apc.bundles;

        for (int i = 0; i < bundlesInfo.Length; i++)
        {
            ABEntry entry = new ABEntry();
            entry.bundleInfo = bundlesInfo[i];

            if (!abEntries.ContainsKey(entry.bundleInfo.name))
            {
                abEntries.Add(entry.bundleInfo.name, entry);
            }
        }

        //处理依赖
        List<AssetBundleBuild> abbList = new List<AssetBundleBuild>();

        foreach (var rEntryItem in abEntries)
        {
            HandleDependencies(rEntryItem.Value);
        }

        abbList.AddRange(GetMuliyRefBundleBuild());

        foreach (var rEntryItem in abEntries)
        {
            abbList.AddRange(rEntryItem.Value.ToABBuild());
        }
        return abbList;
    }

    private static Dictionary<string, uint> assetRef;  //资源引用数量
    private static Dictionary<string, List<string>> assetDepenDetail; //资源被引用的prefab列表
    static void HandleDependencies(ABEntry enrty)
    {
        if(assetRef == null) assetRef = new Dictionary<string, uint>();
        if (assetDepenDetail == null) assetDepenDetail = new Dictionary<string, List<string>>();

        string[] assetNames = enrty.GetAllAssets();
        string assetName = string.Empty;
        string[] depens = null;
        string depenAssest = string.Empty;
        string abName = string.Empty;
        for(int i = 0;i < assetNames.Length;i++)
        {
            assetName = assetNames[i];
            abName = enrty.GetAssetABName(assetName);
            depens = AssetDatabase.GetDependencies(assetName);
            for(int j = 0; j < depens.Length;j++)
            {
               depenAssest = depens[j];
               if (!assetRef.ContainsKey(depenAssest))
               {
                    assetRef.Add(depenAssest, 0);
               }
               else
               {
                    assetRef[depenAssest]++;
               }

                if (!assetDepenDetail.ContainsKey(depenAssest))
                    assetDepenDetail[depenAssest] = new List<string>();
                assetDepenDetail[depenAssest].Add(abName);
            }
        }
     
    }

    private static List<AssetBundleBuild> GetMuliyRefBundleBuild()
    {
        List<AssetBundleBuild> multiRefBuilds = new List<AssetBundleBuild>();

        var enu = assetDepenDetail.GetEnumerator();
        while (enu.MoveNext())
        {
            if (enu.Current.Value.Count > 1 && !enu.Current.Key.EndsWith(".cs") && !IsLoopRef(enu.Current.Value))
            {
                Debug.Log(enu.Current.Key + "---" + enu.Current.Value.Count);
                AssetBundleBuild build = new AssetBundleBuild();
                build.assetNames = new string[] { enu.Current.Key };
                build.assetBundleName = Path.GetFileNameWithoutExtension(enu.Current.Key);
                multiRefBuilds.Add(build);
            }
        }

        assetDepenDetail.Clear();
        assetRef.Clear();
        return multiRefBuilds;
    }

    private static bool IsLoopRef(List<string> abList)
    {
        if (abList.Count <= 1) return false;
        string abName = abList[0];
        for(int i = 1; i< abList.Count;i++)
        {
            if (abList[i] != abName) return false;
        }
        return true;
    }

    private class ABEntry
    {
        public AssetBundlePackageInfo bundleInfo;

        public AssetBundleBuild[] ToABBuild()
        {
            switch (bundleInfo.packageType)
            {
                case "Dir_Dir":
                    return GetOneDir_Dirs();
                case "Dir_File":
                    return GetOneDir_Files();
                case "Dir":
                    return GetOneDir();
                case "File":
                    return GetOneFile();
            }
            return null;
        }

        public string[] GetAllAssets()
        {
            switch (bundleInfo.packageType)
            {
                case "Dir_Dir":
                    return GetOneDirAssets();
                case "Dir_File":
                    return GetOneDirAssets();
                case "Dir":
                    return GetOneDirAssets();
                case "File":
                    return GetOneFileAssets();
            }
            return null;
        }

        public string GetAssetABName(string assetName)
        {

            switch (bundleInfo.packageType)
            {
                case "Dir_Dir":
                    DirectoryInfo rDirInfo = new DirectoryInfo(bundleInfo.assetPath);
                    if (!rDirInfo.Exists) return null;

                    DirectoryInfo[] rSubDirs = rDirInfo.GetDirectories();
                    for (int i = 0; i < rSubDirs.Length; i++)
                    {
                        var files = rSubDirs[i].GetFiles();
                   

                        for (int j = 0; j < files.Length;j++)
                        {
                            var rFileName = files[j].FullName;
                            if (Application.platform == RuntimePlatform.WindowsEditor)
                            {
                                string rRootPath = System.Environment.CurrentDirectory + "\\";
                                rFileName = rFileName.Replace(rRootPath, "").Replace("\\", "/");
                            }else if(Application.platform == RuntimePlatform.OSXEditor)
                            {
                                string rRootPath = System.Environment.CurrentDirectory + "/";
                                rFileName = rFileName.Replace(rRootPath, "");
                            }
                            if (assetName == rFileName)
                            {
                                string rDirPath = rSubDirs[i].FullName;
                                return rDirPath;
                            }
                        }

                    }
                    return null;
                case "Dir_File":
                    List<string> allABPaths = new List<string>();
                    Util.RecursiveDir(bundleInfo.assetPath, ref allABPaths);


                    for (int i = 0; i < allABPaths.Count; i++)
                    {
                        string rAssetPath = allABPaths[i];
                        if(assetName == rAssetPath)
                        {
                            return bundleInfo.name + "/" + Path.GetFileNameWithoutExtension(rAssetPath);
                        }
                    }
                    return null;
                case "Dir":
                    return bundleInfo.name;
                case "File":
                    return bundleInfo.name;
                default:
                    return null;
            }
        }

        /// <summary>
        /// 得到一个文件的ABB
        /// </summary>
        private AssetBundleBuild[] GetOneFile()
        {
            Object rObj = AssetDatabase.LoadAssetAtPath(bundleInfo.assetPath, typeof(Object));
            if (rObj == null) return null;

            AssetBundleBuild rABB = new AssetBundleBuild();
            rABB.assetBundleName = bundleInfo.name;
            rABB.assetNames = new string[] { bundleInfo.assetPath };
            //rABB.assetBundleVariant = Global.BundleExtName;
            return new AssetBundleBuild[] { rABB };
        }

        private string[] GetOneFileAssets()
        {
            Object rObj = AssetDatabase.LoadAssetAtPath(bundleInfo.assetPath, typeof(Object));
            if (rObj == null) return null;
            string[] assets = new string[] { bundleInfo.assetPath };
            return assets;
        }

        /// <summary>
        /// 得到一个目录的ABB
        /// </summary>
        private AssetBundleBuild[] GetOneDir()
        {
            DirectoryInfo rDirInfo = new DirectoryInfo(bundleInfo.assetPath);
            if (!rDirInfo.Exists) return null;

            AssetBundleBuild rABB = new AssetBundleBuild();
            rABB.assetBundleName = bundleInfo.name;
            rABB.assetNames = new string[] { bundleInfo.assetPath };
           // rABB.assetBundleVariant = Global.BundleExtName;
            return new AssetBundleBuild[] { rABB };
        }

        private string[] GetOneDirAssets()
        {
            DirectoryInfo rDirInfo = new DirectoryInfo(bundleInfo.assetPath);
            if (!rDirInfo.Exists) return null;

            List<string> allABPaths = new List<string>();
            Util.RecursiveDir(bundleInfo.assetPath, ref allABPaths);
            string[] assets = new string[allABPaths.Count];
            for(int i = 0;i < allABPaths.Count;i++)
            {
                assets[i] = allABPaths[i];
            }
            return assets;
        }

        /// <summary>
        /// 得到一个目录下的所有的文件对应的ABB
        /// </summary>
        private AssetBundleBuild[] GetOneDir_Files()
        {
            DirectoryInfo rDirInfo = new DirectoryInfo(bundleInfo.assetPath);
            if (!rDirInfo.Exists) return null;

            List<string> allABPaths = new List<string>();
            Util.RecursiveDir(bundleInfo.assetPath, ref allABPaths);

            List<AssetBundleBuild> rABBList = new List<AssetBundleBuild>();

            for (int i = 0; i < allABPaths.Count; i++)
            {
                string rAssetPath = allABPaths[i];
                AssetBundleBuild rABB = new AssetBundleBuild();
                rABB.assetBundleName = bundleInfo.name + "/" + Path.GetFileNameWithoutExtension(rAssetPath);
                rABB.assetNames = new string[] { rAssetPath };
                //rABB.assetBundleVariant = Global.BundleExtName;
                rABBList.Add(rABB);
            }
            
            return rABBList.ToArray();

        }

        /// <summary>
        /// 得到一个目录下的所有的目录对应的ABB
        /// </summary>
        private AssetBundleBuild[] GetOneDir_Dirs()
        {
            DirectoryInfo rDirInfo = new DirectoryInfo(bundleInfo.assetPath);
            if (!rDirInfo.Exists) return null;

            List<AssetBundleBuild> rABBList = new List<AssetBundleBuild>();
            DirectoryInfo[] rSubDirs = rDirInfo.GetDirectories();
            for (int i = 0; i < rSubDirs.Length; i++)
            {
                string rDirPath = rSubDirs[i].FullName;
                string rFileName = "";
                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    string rRootPath = System.Environment.CurrentDirectory + "\\";
                    rDirPath = rDirPath.Replace(rRootPath, "").Replace("\\", "/");
                    rFileName = Path.GetFileNameWithoutExtension(rDirPath);
                }
                else if (Application.platform == RuntimePlatform.OSXEditor)
                {
                    string rRootPath = System.Environment.CurrentDirectory + "/";
                    rDirPath = rDirPath.Replace(rRootPath, "");
                    rFileName = Path.GetFileNameWithoutExtension(rDirPath);
                }
                else
                {
                    throw new System.NotSupportedException("当前运行平台不支持打包操作");
                }

                AssetBundleBuild rABB = new AssetBundleBuild();
                rABB.assetBundleName = bundleInfo.name + "/" + rFileName;
                rABB.assetNames = new string[] { rDirPath };
                //rABB.assetBundleVariant = Global.BundleExtName;
                rABBList.Add(rABB);
            }
            return rABBList.ToArray();
        }
    }

    class AssetBuildInfo
    {
       private List<string> refList = new List<string>();
       private string assetName;
       public AssetBuildInfo(string asset)
       {
            assetName = asset;
       }


        public void AddRef(string refName)
        {

        }
    }
}