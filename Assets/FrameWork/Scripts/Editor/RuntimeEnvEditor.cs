﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class RuntimeEnvEditor
{
    /// <summary>
    /// 是否开启debu
    /// </summary>
    const string DEBUG_SYMBOL = "YL_DEBUG";
    /// <summary>
    /// 是否为内网测试环境
    /// </summary>
    const string TEST_ENV_SYMBOL = "YL_TEST_ENV";

    public static BuildTargetGroup[] platforms = new BuildTargetGroup[] { BuildTargetGroup.Standalone, BuildTargetGroup.Android, BuildTargetGroup.iOS };
    public static string[] ylSymbols = new string[] { DEBUG_SYMBOL, TEST_ENV_SYMBOL };

    [MenuItem("Tools/RuntimeEnvSetting")]
    static void OpenWindow()
    {
        RuntimeEvnSettingWin win = ScriptableObject.CreateInstance<RuntimeEvnSettingWin>();
        win.Show();
    }

    /// <summary>
    /// 设置当前打包平台的宏定义
    /// </summary>
    /// <param name="debug">是否开启debug</param>
    /// <param name="testEnv">是否为测试环境</param>
    public static void SwitchCrtPackgePlatformSymbol(bool debug,bool testEnv)
    {
        Debug.Log("处理当前打包所需宏定义!!!");
        BuildTargetGroup targetGroup = PackagePlatform.GetBuildTargetGroup();
        SwitchSybol(DEBUG_SYMBOL, targetGroup, debug);
        SwitchSybol(TEST_ENV_SYMBOL, targetGroup, testEnv);
    }

    /// <summary>
    /// 设置是否定义指定的宏定义
    /// </summary>
    /// <param name="symbol">宏定义名称</param>
    /// <param name="group">平台</param>
    /// <param name="enable">是否定义</param>
    public static void SwitchSybol(string symbol,BuildTargetGroup group,bool enable)
    {
        string symbolString = PlayerSettings.GetScriptingDefineSymbolsForGroup(group);
        List<string> symbols = new List<string>(symbolString.Split(';'));
        if(enable)
        {
            if (symbols.Contains(symbol)) return;
            symbols.Add(symbol);
            string newSymbolStr = "";
            string suffix = ";";
            for (int i = 0; i < symbols.Count; i++)
            {
                suffix = i == symbols.Count - 1 ? "" : ";";
                newSymbolStr += symbols[i] + suffix;
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(group, newSymbolStr);
        }
        else
        {
            if (!symbols.Contains(symbol)) return;
            symbols.Remove(symbol);
            string newSymbolStr = "";
            string suffix = ";";
            for (int i = 0; i < symbols.Count; i++)
            {
                suffix = i == symbols.Count - 1 ? "" : ";";
                newSymbolStr += symbols[i] + suffix;
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(group, newSymbolStr);
        }

        AssetDatabase.SaveAssets();
        //AssetDatabase.Refresh();
    }

    public static bool GetPlatformSymbolFlag(string symbol, BuildTargetGroup group)
    {
        string symbolString = PlayerSettings.GetScriptingDefineSymbolsForGroup(group);
        List<string> symbols = new List<string>(symbolString.Split(';'));
        return symbols.Contains(symbol);
    }
}

public class RuntimeEvnSettingWin:EditorWindow
{
    private bool[] flags;
    void OnEnable()
    {
        flags = new bool[RuntimeEnvEditor.ylSymbols.Length * RuntimeEnvEditor.platforms.Length];
        int index = 0;
        for (int i = 0; i < RuntimeEnvEditor.ylSymbols.Length; i++)
        {
            for (int j = 0; j < RuntimeEnvEditor.platforms.Length; j++)
            {
                index = i * RuntimeEnvEditor.platforms.Length + j;
                flags[index] = RuntimeEnvEditor.GetPlatformSymbolFlag(RuntimeEnvEditor.ylSymbols[i], RuntimeEnvEditor.platforms[j]);
            }
        }
    }

    void OnGUI()
    {
        GUILayout.BeginVertical();
        int index;
        for (int k = 0; k < RuntimeEnvEditor.ylSymbols.Length;k++)
        {
            GUILayout.BeginVertical();
            GUILayout.Label(RuntimeEnvEditor.ylSymbols[k]);
            GUILayout.Box("", GUILayout.Width(this.position.width), GUILayout.Height(1));
            GUILayout.Box("", GUILayout.Width(this.position.width), GUILayout.Height(1));
            for (int i = 0; i < RuntimeEnvEditor.platforms.Length; i++)
            {
                index = k * RuntimeEnvEditor.platforms.Length + i;
                GUILayout.BeginHorizontal();
                GUILayout.Label(RuntimeEnvEditor.platforms[i].ToString());
                flags[index] = GUILayout.Toggle(flags[index], "");
                GUILayout.EndHorizontal();
            }
        }
    
        GUILayout.EndVertical();

        GUILayout.BeginHorizontal();
        GUILayout.Space(this.position.width - 100 - 50);
        if (GUILayout.Button("Set", GUILayout.Width(100), GUILayout.Height(50)))
        {
            SaveConfig();
        }
        GUILayout.EndHorizontal();

    }

    void SaveConfig()
    {
        int index = 0;
        for (int i = 0; i < RuntimeEnvEditor.ylSymbols.Length; i++)
        {
            for (int j = 0; j < RuntimeEnvEditor.platforms.Length; j++)
            {
                index = i * RuntimeEnvEditor.platforms.Length + j;
                RuntimeEnvEditor.SwitchSybol(RuntimeEnvEditor.ylSymbols[i], RuntimeEnvEditor.platforms[j],flags[index]);
            }
        }
    }
}
