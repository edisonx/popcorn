﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ClearCache
{
    [MenuItem("Tools/ClearCache")]
    static void Clear()
    {
        if(Caching.CleanCache())
        {
            Debug.Log("Clear Cache Succees!!!");
        }
        else
        {
            Debug.Log("Clear Cache fail!!!");
        }
    }

    [MenuItem("Tools/ClearToken")]
    static void ClearToken()
    {
        PlayerPrefs.DeleteKey("Token");
        PlayerPrefs.DeleteKey("UID");
        Debug.Log("Clear token Succees!!!");
    }

}
