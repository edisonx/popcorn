﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using UnityEngine.Events;
using System.Text.RegularExpressions;

public class GameController : MonoBehaviour {
    private GameController() { }
    private static GameController _instance;
    public static GameController Instance {
        get {
            if (null == _instance) {
                string rName = "MainTick";
                GameObject main = GameObject.Find(rName);
                if (main == null) {
                    main = new GameObject(rName);
                    main.name = rName;
                    main.tag = rName;
                }
                _instance = main.GetComponentSafe<GameController>();
            }

            return _instance;
        }
    }

    object scriptGameInstance = null;
    Action onStartupFunc;

    public void Initialize(Action onComplete) {
        if (onComplete != null) {
            onStartupFunc = onComplete;
        }

        //取消 Destroy 对象 
        DontDestroyOnLoad(gameObject);
#if YL_DEBUG
        InitConsole();
#endif
        InitUIRoot();
        InitResolution();

        //平台初始化
        AppPlatform.Initialize();

        //基本设置
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = Global.FrameRate;
        UnityEngine.QualitySettings.vSyncCount = Global.VSyncCount;

        //挂载管理器并初始化
        ManagerCollect.Instance.AddManager<TaskManager>(ManagerName.Task);
        ManagerCollect.Instance.AddManager<AssetLoadManager>(ManagerName.AssetLoad);
        ManagerCollect.Instance.AddManager<SoundManager>(ManagerName.Sound);
        ManagerCollect.Instance.AddManager<GestureManager>(ManagerName.Gesture);
        InitTemplate();
        //创建运行时资源目录
        FileUtil.CreateFolder(AppPlatform.RuntimeAssetsPath);

        AssetsUpdater.Run(() => {
            LoadAssetbundleManifest();
        });
    }

#if YL_DEBUG
    void InitConsole() {
        string rName = "DebugConsloe";
        GameObject consloe = GameObject.Find(rName);
        if (consloe == null) {
            consloe = new GameObject(rName);
            consloe.name = rName;
        }
        var com = consloe.GetComponentSafe<DebugConsole>();
        com.IsDebugMode = true;
        com.isOpenBriefView = true;
    }
#endif

    void InitUIRoot() {
        string rName = "UIRoot";
        GameObject root = GameObject.Find(rName);

        if (root == null) {
            var temp = Resources.Load<GameObject>(rName);

            temp.name = rName;
            root = GameObject.Instantiate(temp);
            root.name = rName;
        }
        root.transform.Reset();
        DontDestroyOnLoad(root);
    }

    void InitResolution() {
        /*
#if UNITY_ANDROID
        if (scaleWidth == 0 && scaleHeight == 0)
        {
            //1080 1920
            //640  1136
            int width = Screen.currentResolution.width;
            int height = Screen.currentResolution.height;
            int designWidth = (int)AppConst.ReferenceResolution.x;
            int designHeight = (int)AppConst.ReferenceResolution.y;
            float s1 = (float)designWidth / (float)designHeight;
            float s2 = (float)width / (float)height;
            if (s1 < s2)
            {
                designWidth = (int)Mathf.FloorToInt(designHeight * s2);
            }
            else if (s1 > s2)
            {
                designHeight = (int)Mathf.FloorToInt(designWidth / s2);
            }
            float contentScale = (float)designWidth / (float)width;
            if (contentScale < 1.0f)
            {
                scaleWidth = designWidth;
                scaleHeight = designHeight;
            }
        }

        if (scaleWidth > 0 && scaleHeight > 0)
        {
            if (scaleWidth % 2 == 0)
            {
                scaleWidth += 1;
            }
            else
            {
                scaleWidth -= 1;
            }
            Screen.SetResolution(scaleWidth, scaleHeight, false);
        }
#endif
        */
    }

    private void InitTemplate() {
        try {
            AssetBundle fontAb = AssetBundle.LoadFromFile(AppPlatform.GetPackagePath() + AppPlatform.GetAssetBundleDirName() + "/msyh");
            Global.AssetLoadManager.SetFontAssetBundle("msyh", fontAb);

            AssetBundle templateAb = AssetBundle.LoadFromFile(AppPlatform.GetPackagePath() + AppPlatform.GetAssetBundleDirName() + "/ui/template");
            templateAb.LoadAllAssets();
            var templates = templateAb.LoadAllAssets<GameObject>();

            string rName = "UIRoot";
            GameObject root = GameObject.Find(rName);
            Transform templateRoot = root.transform.Find("MessageCanvas/Templates");

            GameObject tempalte = null;
            for (int i = 0; i < templates.Length; i++) {
                tempalte = GameObject.Instantiate(templates[i], templateRoot);
                tempalte.name = tempalte.name.Replace("(Clone)", "");
                if (tempalte.name == TemplateName.DialogBox) {
                    var dialog = tempalte.AddComponent<DialogBox>();
                    DialogBox.Templates.AddTemplate(TemplateName.DialogBox, dialog);
                }
            }
        } catch (Exception e) {
            DebugConsole.Log(e.Message);
        }
    }

    public void SetScriptGameInstance(object rObj) {
        if (rObj != null) {
            scriptGameInstance = rObj;
        }
    }

    void LoadAssetbundleManifest() {
        LoadingLayer.SetProgressbarTips("游戏初始化中");
        LoadingLayer.SetProgressbarValue(10);
        IEnumerator progressContinue = UpdateProgressBar();
        StartCoroutine(progressContinue);
        string bundlName = AppPlatform.GetAssetBundleDirName();
        Global.AssetLoadManager.DownloadingURL = AppPlatform.GetRuntimeAssetBundleUrl();
#if YL_TEST_ENV
        string host = Global.ServerConfig.GetString("Server", "debugResHost");
#else
        string host = Global.ServerConfig.GetString("Server", "resHost");
#endif

        Global.resServerURL = host;
        Global.AssetLoadManager.ResServerURL = Global.resServerURL + AppPlatform.GetServerAssetBundleUrl();
        Global.AssetLoadManager.LoadManifest(bundlName, () => {
            ResourceManager.Instance.InitLocalConfig();
            StopCoroutine(progressContinue);
            LoadingLayer.SetProgressbarValue(100);
            CheckNetwork();
        });
    }

    IEnumerator UpdateProgressBar() {
        int progress = 10;
        while (progress < 100) {
            LoadingLayer.SetProgressbarValue(progress++);
            yield return new WaitForEndOfFrame();
        }
    }

    void CheckNetwork() {
        LoadingLayer.SetProgressbarTips("检查网络连接");
        if (!NetWorkMananger.Instance.NetWorkAvailable()) {
            DialogBox.Template(TemplateName.DialogBox).Show(buttons: new DialogActions()
            {
                            {"确认", () =>{StartCoroutine( RetryNetwork()); return true; }},
                            {"取消",()=> { StartCoroutine( RetryNetwork()); return true; } },
            },
            message: "您当前网络信号不好，请移动到网络良好的区域");
            return;
        } else {
            CheckVersion();
        }
    }

    IEnumerator RetryNetwork() {
        yield return new WaitForSeconds(5);
        CheckNetwork();
    }

    void CheckVersion() {
#if UNITY_EDITOR
        LoadSeverResConfig();
#else
        LoadingLayer.SetProgressbarTips("校验版本");
#if YL_TEST_ENV
        string url = Global.ServerConfig.GetString("Server", "debugHost") + "v1/sys/startUp";
#else
        string url = Global.ServerConfig.GetString("Server", "host") + "v1/sys/startUp";
#endif
        UnityWebClientSingleton.Instance.Get(url, req => {
            if (req.responseCode == 200) {
                IDictionary temp = LitJson.MiniJSON.Deserialize(req.downloadHandler.text) as IDictionary;
                if (temp != null) {
                    IDictionary returnData = temp["returnData"] as IDictionary;
                    IDictionary versionInfo = returnData["version"] as IDictionary;
                    if (versionInfo != null) {
                        string version = versionInfo["number"] as String;
                        string[] verStr = version.Split('.');
                        Regex reg = new Regex(@"^[1-9]\d*\.\d*|0\.\d*[1-9]\d*$");
                        string crtVersion = Application.version;
                        Match match = reg.Match(crtVersion);
                        crtVersion = match.ToString();
                        string[] crtVerStr = crtVersion.Split('.');
                        if (Convert.ToInt32(verStr[0]) > Convert.ToInt32(crtVerStr[0]) || Convert.ToInt32(verStr[1]) > Convert.ToInt32(crtVerStr[1])) // 大版本不一致
                        {
                            string updateURL = versionInfo["dowload"] as string;
                            DialogBox.Template(TemplateName.DialogBox).Show(buttons: new DialogActions()
                            {
                                {"确认", () =>{ Application.OpenURL(updateURL); return true; }},
                                {"取消",()=> {
                                    Application.Quit();
                                    return true;
                                } },
                            },
                             message: "当前版本过低，请更新最新版本");
                        } else {
                            LoadSeverResConfig();
                        }
                    } else {
                        LoadSeverResConfig();
                    }
                } else {
                    LoadingLayer.SetProgressbarTips("校验版本失败");
                }
            } else {
                LoadingLayer.SetProgressbarTips("校验版本失败");
                print("读取资源配置失败" + req.responseCode);
            }
        });
#endif
    }

    void LoadSeverResConfig() {
        LoadingLayer.SetProgressbarTips("读取资源配置");
        string serverResConfigUrl = Global.resServerURL + AppPlatform.PlatformCurrentName.ToLower() + "/res/1001/server_res_config.txt";
        print(serverResConfigUrl);
        UnityWebClientSingleton.Instance.Get(serverResConfigUrl, req => {
            if (req.responseCode == 200) {
                StringReader sr = new StringReader(req.downloadHandler.text);
                string config = string.Empty;
                string[] temp = null;
                while (sr.Peek() > -1) {
                    config = sr.ReadLine();
                    temp = config.Split('|');
                    if (temp.Length != 2) continue;
                    Global.AssetLoadManager.SetServerAssetBundleConfig(temp[0], Convert.ToUInt32(temp[1]));
                }
                sr.Close();
                sr.Dispose();
                sr = null;
                onStartupFunc();
            } else {
                print("读取资源配置失败" + req.responseCode);
                LoadingLayer.SetProgressbarTips("读取资源配置失败");
            }
        });
    }

    /// <summary>
    /// 退出游戏
    /// </summary>
    void GameEnd() {
        //LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "End");
        scriptGameInstance = null;
        Global.AssetLoadManager.UnloadAssetBundles();
        UIUtil.ClearUICache();
        Util.ClearMemory();
        //Caching.CleanCache();

    }

    void OnApplicationQuit() {
        GameEnd();
    }

    void Update() {
        if (scriptGameInstance != null)
            LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "Update");
    }

    void FixedUpdate() {
        if (scriptGameInstance != null)
            LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "FixedUpdate");
    }

    void LateUpdate() {
        if (scriptGameInstance != null)
            LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "LateUpdate");
    }
}
