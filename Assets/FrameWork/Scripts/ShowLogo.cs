﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class ShowLogo : MonoBehaviour {

    public Image img;
    public Image bg;
    private bool isFadeoutEnd = false;
    private bool mainSceneIsLoaded = false;
	void Start ()
    {
#if UNITY_EDITOR
        isFadeoutEnd = true;
        StartCoroutine(LoadMainScene());
#else
        StartCoroutine(FadeOut());
#endif
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(2);
        DOTween.ToAlpha(
            () => { return img.color; },
            (val) => {
                img.color = val;
                bg.color = val;
            }, 0f, 2).OnComplete(()=> {
            isFadeoutEnd = true;
            UnLoadStartUpScene();
        });

        StartCoroutine(LoadMainScene());
    }

    IEnumerator LoadMainScene()
    {
       AsyncOperation asyncOpt = SceneManager.LoadSceneAsync("main", LoadSceneMode.Additive);
       yield return asyncOpt;
        mainSceneIsLoaded = true;
        UnLoadStartUpScene();
    }

    void UnLoadStartUpScene()
    {
        if (!mainSceneIsLoaded || !isFadeoutEnd) return;

        SceneManager.UnloadSceneAsync("Startup");
        GameObject go = GameObject.Find("Launcher");
        Launcher lan = go.GetComponent<Launcher>();
        lan.enabled = true;
    }
}
