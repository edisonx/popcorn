﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

#if UNITY_5_4_OR_NEWER
using UnityEngine.Networking;
#elif UNITY_5_2 || UNITY_5_3
using UnityEngine.Experimental.Networking;
#else
#error It requires Unity 5.2.0 or higher.
#endif

public class UnityWebClient : MonoBehaviour {

    protected CookieContainer _cookies = new CookieContainer();

    public CookieContainer Cookies {
        get {
            return _cookies;
        }
    }

    /// <summary>
    /// The headers.
    /// </summary>
    public static Dictionary<string, string> Headers = new Dictionary<string, string>();

    /// <summary>
    /// Dispose request after each a request hadnler.
    /// </summary>
    public bool AutoDisposeRequest {
        get;
        set;
    }

    /// <summary>
    /// Sets the headers.
    /// X-AUTH-TOKEN	用户登录后代表用户唯一身份的凭证,客户端在调用登录或注册接口后获得此值
    /// X-CHANNEL-ID	渠道号
    /// X-CLIENT-VERSION	客户端版本号
    /// X-MACHINE-ID	设备唯一码
    public void SetHeaders() {
        Headers.Add("X-AUTH-TOKEN", "");
        Headers.Add("X-CHANNEL-ID", "");
        Headers.Add("X-CLIENT-VERSION", "");
        Headers.Add("X-MACHINE-ID", SystemInfo.deviceUniqueIdentifier);
    }

    public void SetHeader(string key, string val) {
        if (!Headers.ContainsKey(key)) {
            Headers.Add(key, val);
        }
        else {
            Headers[key] = val;
        }
    }

    /// <summary>
    /// Cs the request.
    /// </summary>
    /// <returns>The request.</returns>
    /// <param name="request">Request.</param>
    /// <param name="handler">Handler.</param>
    IEnumerator CRequest(UnityWebRequest request, Action<UnityWebRequest> handler) {
        if (_cookies.Contains(request.url)) {
            request.SetRequestHeader("cookie", _cookies[request.url].ToString());
        }

        //set header
        if (Headers.Count > 0) {
            var enu = Headers.GetEnumerator();
            while (enu.MoveNext()) {
                request.SetRequestHeader(enu.Current.Key, enu.Current.Value);
            }
        }

        int redirectLimit = request.redirectLimit;
        int redirectCount = 0;
        request.redirectLimit = 0;
        while (redirectCount <= redirectLimit) {
            Debug.Log(redirectCount + "    count : limit    " + redirectLimit);
            if (_cookies.Contains(request.url)) {
                request.SetRequestHeader("cookie", _cookies[request.url].ToString());
            }
            yield return request.Send();
            string cookieString = request.GetResponseHeader("set-cookie");
            if (!String.IsNullOrEmpty(cookieString))
                _cookies.Set(request.url, cookieString);
            if (request.responseCode == 301 || request.responseCode == 302) {
                var newRequest = new UnityWebRequest(new Uri(request.url).GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped)
                                 + request.GetResponseHeader("location"),
                                     request.method,
                                     request.downloadHandler,
                                     request.uploadHandler);
                request.disposeDownloadHandlerOnDispose = false;
                request.disposeUploadHandlerOnDispose = false;
                request.Dispose();
                request = newRequest;
                redirectCount++;
            }
            else
                break;
        }
        if (handler != null)
            handler(request);
        if (AutoDisposeRequest)
            request.Dispose();
    }

    /// <summary>
    /// Request the specified request and handler.
    /// </summary>
    /// <param name="request">Request.</param>
    /// <param name="handler">Handler.</param>
    public void Request(UnityWebRequest request, Action<UnityWebRequest> handler) {
        StartCoroutine(CRequest(request, handler));
    }

    /// <summary>
    /// Get the specified uri and handler.
    /// </summary>
    /// <param name="uri">URI.</param>
    /// <param name="handler">Handler.</param>
    public void Get(string uri, Action<UnityWebRequest> handler) {
        StartCoroutine(CRequest(UnityWebRequest.Get(uri), handler));
    }

    /// <summary>
    /// Post the specified uri, formFields and handler.
    /// </summary>
    /// <param name="uri">URI.</param>
    /// <param name="formFields">Form fields.</param>
    /// <param name="handler">Handler.</param>
    public void Post(string uri, IDictionary<string, string> formFields, Action<UnityWebRequest> handler) {
        UnityWebRequest www;

        if (formFields == null || formFields.Count == 0)
            www = UnityWebRequest.Get(uri);
        else
            www = UnityWebRequest.Post(uri, new Dictionary<string, string>(formFields));

        StartCoroutine(CRequest(www, handler));
    }

    /// <summary>
    /// Post the specified uri, postData and handler.
    /// </summary>
    /// <param name="uri">URI.</param>
    /// <param name="postData">Post data.</param>
    /// <param name="handler">Handler.</param>
    public void Post(string uri, byte[] postData, Action<UnityWebRequest> handler) {
        UnityWebRequest www;

        if (postData == null || postData.Length == 0)
            www = UnityWebRequest.Get(uri);
        else {
            www = new UnityWebRequest(uri, UnityWebRequest.kHttpVerbPOST);
            www.uploadHandler = new UploadHandlerRaw(postData);
            www.downloadHandler = new DownloadHandlerBuffer();
            //www.method = UnityWebRequest.kHttpVerbPOST;
            www.SetRequestHeader("Content-Type", "application/octet-stream");
        }

        StartCoroutine(CRequest(www, handler));
    }

    /// <summary>
    /// Post the specified uri, postData and handler.
    /// </summary>
    /// <param name="uri">URI.</param>
    /// <param name="postData">Post data.</param>
    /// <param name="handler">Handler.</param>
    public void Post(string uri, string postData, Action<UnityWebRequest> handler) {
        UnityWebRequest www;

        if (postData == null || String.IsNullOrEmpty(postData))
            www = UnityWebRequest.Get(uri);
        else {
            //www = new UnityWebRequest(uri,UnityWebRequest.kHttpVerbPOST);
            www = new UnityWebRequest(uri, UnityWebRequest.kHttpVerbPOST);
            var data = System.Text.Encoding.UTF8.GetBytes(postData);
            www.uploadHandler = new UploadHandlerRaw(data);
            www.downloadHandler = new DownloadHandlerBuffer();
            //www.method = UnityWebRequest.kHttpVerbPOST;
            www.SetRequestHeader("Content-Type", "application/json");
        }

        StartCoroutine(CRequest(www, handler));
    }

    /// <summary>
    /// Creates and sends a UnityWebReqeust configured for PUT
    /// </summary>
    public void Put(string uri, byte[] bodyData, Action<UnityWebRequest> handler) {
        UnityWebRequest www;

        if (bodyData == null || bodyData.Length == 0)
            www = UnityWebRequest.Get(uri);
        else {
            www = new UnityWebRequest(uri, UnityWebRequest.kHttpVerbPUT);
            www.uploadHandler = new UploadHandlerRaw(bodyData);
            www.downloadHandler = new DownloadHandlerBuffer();
            www.SetRequestHeader("Content-Type", "application/json");
        }

        StartCoroutine(CRequest(www, handler));
    }

    /// <summary>
    /// Creates and sends a UnityWebReqeust configured for PUT with UTF8
    /// </summary>
    public void Put(string uri, string bodyData, Action<UnityWebRequest> handler) {
        Put(uri, System.Text.Encoding.UTF8.GetBytes(bodyData), handler);
    }

    /// <summary>
    /// Creates and sends a UnityWebReqeust configured for PUT with given encoding
    /// </summary>
    public void Put(string uri, string bodyData, Action<UnityWebRequest> handler, System.Text.Encoding encoding) {
        Put(uri, encoding.GetBytes(bodyData), handler);
    }

    /// <summary>
    /// Creates and sends a UnityWebReqeust configured for DELETE
    /// </summary>
    public void Delete(string uri, Action<UnityWebRequest> handler) {
        StartCoroutine(CRequest(UnityWebRequest.Delete(uri), handler));
    }

    /// <summary>
    /// Creates and sends a UnityWebReqeust configured for HEAD
    /// </summary>
    public void Head(string uri, Action<UnityWebRequest> handler) {
        StartCoroutine(CRequest(UnityWebRequest.Head(uri), handler));
    }

}
