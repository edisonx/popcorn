﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipsPanel : MonoBehaviour {
    public int mTime = 5;
    int mTimer = 0;//计时器

    public Text tipsText;
    public Image[] tipsNumber;

    // Use this for initialization
    void Start() {
        tipsText = transform.Find("TipsText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {

    }

    public IEnumerator StartTime(Action func) {
        mTimer = mTime;

        gameObject.SetActive(true);
        tipsText.gameObject.SetActive(true);
        int num = Global.GameManager.GAME_CUR_INDEX;
        tipsText.text = string.Format("第{0}-{1}局", num + 1, Global.GameManager.GAME_NUM_MAX);
        for (int i = 0; i < tipsNumber.Length; i++) {
            tipsNumber[i].gameObject.SetActive(false);
        }

        Global.GameManager.ActiveBlur(true);
        while (mTimer >= 0) {
            yield return new WaitForSeconds(1); //由于开始倒计时，需要经过一秒才开始减去1秒，
                                                //所以要先用yield return new WaitForSeconds(1);然后再进行mTimer--;运算
            mTimer--;
            if (mTimer > 0) {                   //如果倒计时剩余总时间为0时，就
                ShowNumber(mTimer - 1);
            }
            else {
                func();
                Global.GameManager.ActiveBlur(false);
                gameObject.SetActive(false);
                tipsText.gameObject.SetActive(false);
            }
        }
    }

    void ShowNumber(int num) {
        if (num > tipsNumber.Length)
            return;
        for (int i = 0; i < tipsNumber.Length; i++) {
            if (i == num) {
                tipsNumber[i].gameObject.SetActive(true);
            }
            else {
                tipsNumber[i].gameObject.SetActive(false);
            }
        }
    }
}
