﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public enum UserState {
    Starting,
    Playing,
    Dying,
    Waiting,
}

public class User {

    #region Private Properties

    private int mSocketId = -1;
    private int mUserId = 0;
    private int mScore;
    private int mHealth;
    private string mNickName = string.Empty;
    private string mThumbUrl = string.Empty;
    private byte[] mThumbData;
    private Texture2D mThumbTex2D;
    private Image mThumbPng;
    private GameObject mPlayerGO;
    private UserState mUserState;
    int mIndex = -1;

    #endregion

    #region Public Properties

    public int SocketID {
        get { return mSocketId; }
        set { mSocketId = value; }
    }

    public int UserID {
        get { return mUserId; }
        set { mUserId = value; }
    }

    public int Score {
        get { return mScore; }
        set { mScore = value; }
    }

    public int Health {
        get { return mHealth; }
        set { mHealth = value; }
    }

    public string NickName {
        get { return mNickName; }
        set { mNickName = value; }
    }

    public string ThumbUrl {
        get { return mThumbUrl; }
        set { mThumbUrl = value; }
    }

    public byte[] ThumbData {
        get { return mThumbData; }
        set { mThumbData = value; }
    }

    public Texture2D ThumbTex2D {
        get { return mThumbTex2D; }
        set { mThumbTex2D = value; }
    }

    public Image ThumbPng {
        get { return mThumbPng; }
        set { mThumbPng = value; }
    }

    public GameObject PlayerGameObject {
        get { return mPlayerGO; }
        set { mPlayerGO = value; }
    }

    public UserState UserState {
        get { return mUserState; }
        set { mUserState = value; }
    }

    #endregion

    public User (int socketId, int userId, string nickName, string thumbUrl) {
        this.mSocketId = socketId;
        this.mUserId = userId;
        this.mNickName = nickName;
        this.mThumbUrl = thumbUrl;
        //TODO 需要下载头像 但是不能直接在构造函数中执行
        //Global.GameManager.StartCoroutine(downloadImage(mThumbUrl, mThumbPng));
    }

    public void addScore (int score) {
        mScore += score;
        NetworkControler.Instance.updateUserScore(mSocketId, mScore);
    }

    public void changeUserState (UserState state) {
        if (state != mUserState) {
            mUserState = state;
        }
    }

    public void release () {
        //TODO 作为释放
        PlayerCtrl player = mPlayerGO.GetComponent<PlayerCtrl>();
        player.die();
    }

    public IEnumerator downloadImage (string url, Action callback) {
        //DebugConsole.Log("downloading new image:" + Application.persistentDataPath + url.GetHashCode());//url转换HD5作为名字
        if (url != null)
            mThumbUrl = url;
        WWW www = new WWW(mThumbUrl);
        yield return www;

        mThumbTex2D = www.texture;
        ////TODO 将图片保存至缓存路径
        //mThumbData = mThumbTex2D.EncodeToPNG();
        //File.WriteAllBytes(Application.persistentDataPath + url.GetHashCode(), mThumbData);

        //Sprite _sprite = Sprite.Create(mThumbTex2D, new Rect(0, 0, mThumbTex2D.width, mThumbTex2D.height), new Vector2(0, 0));
        //ThumbPng.sprite = _sprite;
        callback();
    }

    public void setIndex (int index) {
        mIndex = index;
    }

    public int getIndex () {
        return mIndex;
    }
}
