﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreUI : MonoBehaviour {
    public Text number;
    public Text playerName;
    public Image ThumbPng;
    public Text score;

    public void setThumb (Texture2D texture) {
        ThumbPng.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }

    public void setThumb (string url) {
        ThumbPng.sprite = null;
        StartCoroutine(LoadLocalImage(url));
    }

    private IEnumerator LoadLocalImage (string url) {
        DebugConsole.Log("getting local image:" + url);
        WWW www = new WWW(url);
        yield return www;

        Texture2D texture = www.texture;
        try {
            Sprite m_sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
            ThumbPng.sprite = m_sprite;
        } catch (Exception e) {
            DebugConsole.LogError(e);
            throw;
        }
    }
}
