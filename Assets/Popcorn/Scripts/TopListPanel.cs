﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TopListPanel : MonoBehaviour {
    public List<User> mPlayerList;

    public RectTransform topListBG;

    public Text tipsText;
    public HighscoreUI[] top3Player;
    public RectTransform otherPlayerRoot;

    private Tweener mTweener;//我的动画片

    // Use this for initialization
    void Start () {
        tipsText = transform.Find("TipsText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update () {

    }

    private void OnEnable () {
        //PlayForward();
        InitTopList();
        showTopList();
    }

    private void OnDisable () {

    }

    public void PlayForward () {
        mTweener = topListBG.DOLocalMove(Vector3.zero, 1);
        mTweener.SetAutoKill(false);
    }

    public void PlayBackwards (Action callBack) {
        mTweener.PlayBackwards();
        mTweener.OnComplete(() => callBack());
    }

    public void InitTopList () {
        mPlayerList = new List<User>();

        for (int i = 0; i < top3Player.Length; i++) {
            top3Player[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < otherPlayerRoot.childCount; i++) {

            otherPlayerRoot.GetChild(i).gameObject.SetActive(false);
        }

        tipsText.gameObject.SetActive(true);
    }

    public void showTopList () {
        int num = Global.GameManager.GAME_CUR_INDEX;
        tipsText.text = string.Format("第{0}-{1}局", num, Global.GameManager.GAME_NUM_MAX);

        //TODO 测试使用 玩家列表从GameManager中取
        List<PlayerCtrl> localPlayers = Global.GameManager.PlayerRankList;
        if (localPlayers.Count <= 0) {
            return;
        }

        for (int i = 0; i < localPlayers.Count; i++) {
            mPlayerList.Add(localPlayers[i].getUser());
        }

        int localStart = 0;

        int currentHighScore = localStart;

        for (int i = 0; i < mPlayerList.Count; i++) {
            if (i >= top3Player.Length) {
                if (i >= 10)
                    return;
                HighscoreUI hs = otherPlayerRoot.GetChild(currentHighScore).GetComponent<HighscoreUI>();

                if (hs == null) {
                    // We skip the player entry.
                    continue;
                }

                hs.gameObject.SetActive(true);
                hs.number.text = (localStart + i + 1).ToString();
                //hs.playerName.text = mPlayerList[i].NickName;
                hs.playerName.text = RegexUtil.filtrationEmoji(mPlayerList[i].NickName);
                //hs.ThumbPng = mPlayerList[i].ThumbPng;
                //hs.setThumb(mPlayerList[i].ThumbUrl);
                hs.setThumb(mPlayerList[i].ThumbTex2D);
                if (!string.IsNullOrEmpty(mPlayerList[i].Score.ToString())) {
                    //hs.score.gameObject.SetActive(true);
                    hs.score.text = mPlayerList[i].PlayerGameObject.GetComponent<PlayerCtrl>().ToString();
                } else {
                    hs.score.gameObject.SetActive(false);
                }
                currentHighScore++;
            } else {
                HighscoreUI hs = top3Player[i];
                hs.gameObject.SetActive(true);
                //hs.playerName.text = mPlayerList[i].NickName;
                hs.playerName.text = RegexUtil.filtrationEmoji(mPlayerList[i].NickName);
                //hs.ThumbPng = mPlayerList[i].ThumbPng;
                //hs.setThumb(mPlayerList[i].ThumbUrl);
                hs.setThumb(mPlayerList[i].ThumbTex2D);
                if (!string.IsNullOrEmpty(mPlayerList[i].Score.ToString())) {
                    //hs.score.gameObject.SetActive(true);
                    hs.score.text = mPlayerList[i].Score.ToString();
                } else {
                    hs.score.gameObject.SetActive(false);
                }
            }
        }
    }
}
