﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitingItem : MonoBehaviour {
    User mUser;
    public Image mThumbAvatar;
    public Text nickNameLabel;

    void Awake() {
        //获取组件
        mThumbAvatar = transform.Find("ThumbAvatar").GetComponent<Image>();
        nickNameLabel = transform.Find("NickNameLabel").GetComponent<Text>();
    }

    public WaitingItem() {

    }

    public WaitingItem(User user) {
        setWaitingItem(user);
    }

    public void setWaitingItem(User user) {
        mUser = user;

        string nickname = user.NickName;

        nickNameLabel.text = nickname;

        //TODO: 往后需要把头像缓存起来 节省流量
        //StartCoroutine(this.GetHttpImg(user.ThumbUrl, 967));
        mThumbAvatar = user.ThumbPng;
    }

    public int getSocketId() {
        return mUser.SocketID;
    }

    //void GetHttpImg(string imgurl, int number) {
    //    HttpRequest request = new HttpRequest();
    //    // required fields
    //    request.setUrl(imgurl.c_str());
    //    request.setRequestType(HttpRequest.Type.GET);
    //    request.setResponseCallback(this, httpresponse_selector(WaitingItem.onHttpRequestRptImg));
    //    // optional fields
    //    char thisnumber[10] = "";
    //    sprintf(thisnumber, "%d", number);
    //    request.setTag(thisnumber);
    //    HttpClient.getInstance().send(request);
    //    request.release();
    //}

    //void onHttpRequestRptImg(HttpClient sender, HttpResponse response) {

    //    char c_tag[20] = "";
    //    sprintf(c_tag, "%s", response.getHttpRequest().getTag());
    //    DebugConsole.Log("%s completed", response.getHttpRequest().getTag());
    //    string str_tag = c_tag;
    //    if (!response) {
    //        return;
    //    }

    //    if (!response.isSucceed()) {
    //        DebugConsole.Log("response failed");
    //        DebugConsole.Log("error buffer: %s", response.getErrorBuffer());
    //        return;
    //    }

    //    List<char> buffer = response.getResponseData();

    //    //create image
    //    Image img = new Image;
    //    img.initWithImageData((unsigned char *)buffer.data(),buffer.size());

    //    //create texture
    //    Texture2D texture = new Texture2D();
    //    bool isImg = texture.initWithImage(img);
    //    img.release();

    //    if (isImg && texture != null) {
    //        if (mUser) {
    //            mUser.cacheTexture(texture);
    //        }

    //        Utility.createThumbWithTexture(texture, mThumbWidth, mThumbWidth, m_ThumbAvatar, this, false);
    //    }
    //}

    //IEnumerator GetHttpImg(string imgurl, int number) {
    //    WWW www = new WWW(imgurl);
    //    yield return www;

    //    Texture2D tex2d = www.texture;
    //    //将图片保存至缓存路径  
    //    byte[] pngData = tex2d.EncodeToPNG();                         //将材质压缩成byte流
    //    onHttpRequestRptImg(tex2d);

    //    Sprite m_sprite = Sprite.Create(tex2d, new Rect(0, 0, tex2d.width, tex2d.height), new Vector2(0, 0));
    //    mThumbAvatar.sprite = m_sprite;
    //}

    //void onHttpRequestRptImg(Texture2D texture) {
    //    if (texture != null) {
    //        if (mUser != null) {
    //            mUser.cacheTexture(texture);
    //        }
    //    }
    //}
}
