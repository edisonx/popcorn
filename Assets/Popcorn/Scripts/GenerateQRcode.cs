﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.Client.Result;
using ZXing.Common;
using ZXing.QrCode;

public class GenerateQRcode : MonoBehaviour {
    public string qrCodeUrl = "https://cli.im/api/qrcode/code?text={0}&mhid=5EqSDQzsmc0hMHcsKNZTMao";

    public Rect sourceRect = new Rect(24, 24, 210, 210);
    //存放二维码
    public Texture2D encoded;
    public RawImage QRImage;

    // Use this for initialization
    void Start() {
        //StartCoroutine(cli_imQRcode());

        //ShowQRcode();
        //SaveQRcode();
        //ShowQRcode2();
    }

    // Update is called once per frame
    void Update() {

    }

    //void OnGUI() {
    //    GUI.DrawTexture(new Rect(Screen.width - 500, 75, 190, 190), encoded);
    //}

    /// <summary>
    /// 1.生成二维码，使用协程，调用草料API
    /// </summary>
    /// <param name="url">需要生成二维码的链接</param>
    public void ShowQRcode(string url) {
        StartCoroutine(generateQRcode(url));
    }

    public IEnumerator generateQRcode(string url) {
        string qrcodeUrl = string.Format(qrCodeUrl, url);
        WWW www = new WWW(qrcodeUrl);
        yield return www;

        print(www.text);

        //获取'src=" //' 后所有的数据
        string s = www.text.Substring(www.text.IndexOf("<img src=") + 12, www.text.Length - (www.text.IndexOf("<img src=") + 12));
        //截取src="" 内部的链接地址，不包括'//'
        string result = s.Substring(0, s.IndexOf("\""));

        print(result);

        WWW w = new WWW(result);
        yield return w;
        encoded = w.texture;
        QRImage.texture = encoded;
        DebugConsole.LogError("texture.width" + encoded.width + " --**-- texture.height" + encoded.height);
    }

    /// <summary>
    /// 2.生成二维码，
    /// </summary>
    /// <param name="QRcodeStr">需要生成二维码的链接</param>
    /// <param name="func1"></param>
    public void ShowQRcode(string QRcodeStr, int func1) {
        encoded = new Texture2D(256, 256);
        var textForEncoding = QRcodeStr;
        if (textForEncoding != null) {
            //二维码写入图片
            var color32 = Encode(textForEncoding, encoded.width, encoded.height);
            encoded.SetPixels32(color32);
            encoded.Apply();
            //重新赋值一张图，计算大小,避免白色边框过大
            Texture2D encoded1;
            int x = Mathf.FloorToInt(sourceRect.x);
            int y = Mathf.FloorToInt(sourceRect.y);
            int width = Mathf.FloorToInt(sourceRect.width);
            int height = Mathf.FloorToInt(sourceRect.height);
            encoded1 = new Texture2D(width, height);//创建目标图片大小
            encoded1.SetPixels(encoded.GetPixels(x, y, width, height));
            encoded1.Apply();
            QRImage.texture = encoded1;
        }
    }

    /// <summary>
    /// 3.生成二维码，并保存本地
    /// </summary>
    /// <param name="QRcodeStr">需要生成二维码的链接</param>
    /// <param name="func2"></param>
    public void ShowQRcode(string QRcodeStr, string func2) {
        encoded = new Texture2D(256, 256);
        Guid idKey = Guid.NewGuid();
        QRcodeStr = idKey.ToString();
        var textForEncoding = QRcodeStr;
        if (textForEncoding != null) {
            var color32 = Encode(textForEncoding, encoded.width, encoded.height);
            encoded.SetPixels32(color32);
            encoded.Apply();
            QRImage.texture = encoded;
            byte[] bytes = encoded.EncodeToPNG();                                           //把二维码转成byte数组，然后进行输出保存为png图片就可以保存下来生成好的二维码
            if (!Directory.Exists(AppPlatform.RuntimeAssetsPath + "/Edisonx"))              //创建生成目录，如果不存在则创建目录
            {
                Directory.CreateDirectory(AppPlatform.RuntimeAssetsPath + "/Edisonx");
            }
            string fileName = AppPlatform.RuntimeAssetsPath + "/Edisonx/" + idKey + ".png";
            System.IO.File.WriteAllBytes(fileName, bytes);
        }
    }

    private static Color32[] Encode(string textForEncoding, int width, int height) {
        var writer = new BarcodeWriter {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }

    /// <summary>
    /// 4.生成二维码，
    /// </summary>
    /// <param name="QRcodeStr">需要生成二维码的链接</param>
    /// <param name="func3"></param>
    public void ShowQRcode(string QRcodeStr, bool func3) {
        encoded = new Texture2D(512, 512);
        BitMatrix BIT;
        Dictionary<EncodeHintType, object> hints = new Dictionary<EncodeHintType, object>();
        hints.Add(EncodeHintType.CHARACTER_SET, "UTF-8");
        BIT = new MultiFormatWriter().encode(QRcodeStr, BarcodeFormat.QR_CODE, 512, 512, hints);
        int width = BIT.Width;
        int height = BIT.Width;
        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                if (BIT[x, y]) {
                    encoded.SetPixel(y, x, Color.black);
                }
                else {
                    encoded.SetPixel(y, x, Color.white);
                }
            }
        }
        encoded.Apply();
        QRImage.texture = encoded;
    }
}
