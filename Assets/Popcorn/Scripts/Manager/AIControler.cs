﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControler : MonoBehaviour {
    int mIndex = 0;
    public int[] minusSocketId = { -1, -2, -3 };
    public float[] fortifySpeedGears = new[] { 2.0f, 3.0f, 4.5f, 5.0f, 6.5f };
    string[] names = { "№暴雨梨花针", "㊣含笑半步癫", "❦我爱一条柴" };
    //string[] thumbs =
    //{
    //   //"file:///" + Application.streamingAssetsPath + "/avatars/pilot_thumb_0.png",
    //   //"file:///" + Application.streamingAssetsPath + "/avatars/pilot_thumb_1.png",
    //   //"file:///" + Application.streamingAssetsPath + "/avatars/pilot_thumb_2.png",

    //    //"file://" + AppPlatform.RuntimeAssetsPath + "/avatars/pilot_thumb_0.png",
    //    //"file://" + AppPlatform.RuntimeAssetsPath + "/avatars/pilot_thumb_1.png",
    //    //"file://" + AppPlatform.RuntimeAssetsPath + "/avatars/pilot_thumb_2.png",

    //    "http://touxiang.qqzhi.com/uploads/2012-11/1111021933508.jpg",
    //    "http://touxiang.qqzhi.com/uploads/2012-11/1111003209318.jpg",
    //    "http://touxiang.qqzhi.com/uploads/2012-11/1111003753433.jpg"
    //};
    List<string> thumbs;

    private static AIControler _instance;

    public static AIControler Instance {
        get {
            if (_instance == null) {
                _instance = new AIControler();
            }
            return _instance;
        }
    }

    private void Awake () {
        thumbs = new List<string>(){
            "file://" + AppPlatform.RuntimeAssetsPath + "/avatars/pilot_thumb_0.png",
            "file://" + AppPlatform.RuntimeAssetsPath + "/avatars/pilot_thumb_1.png",
            "file://" + AppPlatform.RuntimeAssetsPath + "/avatars/pilot_thumb_2.png"};
    }

    public void addAIPlayer () {
        if (mIndex >= 3) {
            mIndex = 0;
        }
        Global.GameManager.createUser(minusSocketId[mIndex], minusSocketId[mIndex], names[mIndex], thumbs[mIndex]);
        mIndex++;
    }

    public float getSharkCount () {
        int value = Random.Range(0, fortifySpeedGears.Length);
        return fortifySpeedGears[value];
    }
}
