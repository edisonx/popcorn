﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using LitJson;
using SocketIO;
using UnityEngine;
using UnityEngine.Networking;
using WebSocketSharp;
using ErrorEventArgs = WebSocketSharp.ErrorEventArgs;

public class NetworkControler : TSingleton<NetworkControler> {

    #region Public Properties

    //public string GATE_URL = "http://www.91qzb.com/thinkphp/public/index.php/api/index/gate_run";
    public string GATE_URL = "http://www.91qzb.com/dev/public/index.php/api/index/gate_run";
    //public string QR_CODE_URL = "http://www.91qzb.com/thinkphp/public/index.php/api/index/weixin?type=h5&scid={0}&ws={1}&cnmid={2}&pcid={3}&url={4}";
    public string QR_CODE_URL = "http://www.91qzb.com/dev/public/index.php/api/index/weixin?type=h5&scid={0}&ws={1}&cnmid={2}&pcid={3}&url={4}";

    //public string mWSUrl = "ws://114.115.205.206:2360";
    public string mWSUrl;
    public string mScreenId;
    public string mShortTool;
    public string h5;
    public string mPCId = "123";
    public string mCinemaId = "8561";

    public bool autoConnect = true;
    public int reconnectDelay = 5;

    public bool IsConnected { get { return connected; } }

    #endregion

    #region Private Properties

    private volatile bool connected;

    private Thread socketThread;
    private Thread pingThread;

    #endregion

    private NetworkControler () {
        ServicePointManager.ServerCertificateValidationCallback = TrustCertificate;
        //setInit();

        //if (autoConnect) { Connect(); }
    }

    private static bool TrustCertificate (object sender, X509Certificate x509Certificate, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors) {
        // all Certificates are accepted
        return true;
    }

    #region Private Properties

    private WebSocket ws;

    #endregion

    #region Unity interface

    public void AwakeInit () {
        sendHttpRequest();
    }

    public void StartInit () {
        //if (autoConnect) { Connect(); }
    }

    public bool NetWorkAvailable () {
        return Application.internetReachability != NetworkReachability.NotReachable;
    }

    public void connect2Server (string url) {
        ws = new WebSocket(url);
        ws.OnOpen += OnOpen;
        ws.OnMessage += OnMessage;
        ws.OnError += OnError;
        ws.OnClose += OnClose;

        ws.Log.Level = LogLevel.Trace;

        // Connect to the server.
        ws.Connect();

        // Connect to the server asynchronously.
        //ws.ConnectAsync ();

        connected = false;
    }

    #endregion

    #region Public Interface

    public void Connect () {
        connected = true;

        socketThread = new Thread(RunSocketThread);
        socketThread.Start(ws);

        //pingThread = new Thread(RunPingThread);
        //pingThread.Start(ws);
    }

    public void kickUser (int socketId) {
        DebugConsole.Log("kickUser " + Global.GameManager.isGaming());
        if (ws != null && socketId > 0) {
            string strKick = socketId.ToString();
            if (Global.GameManager.isGaming()) {
                strKick += "-kick";
            } else {
                strKick += "-end";
            }
            ws.Send(strKick);
        }
    }

    public void replyUserIamPlaying (int socketId) {
        DebugConsole.Log("replyUserIamPlaying");
        if (ws != null && socketId > 0) {
            string buf = string.Format("{0}-p", socketId);
            ws.Send(buf);
        }
    }

    public void replyUserIamWaiting (int socketId, int n) {
        DebugConsole.Log("replyUserIamWaiting");
        if (ws != null && socketId > 0) {
            string buf = string.Format("{0}-w{1}", socketId, n);
            ws.Send(buf);
        }
    }

    public void updateUserScore (int socketId, int score) {
        DebugConsole.Log("updateUserScore");
        if (ws != null && socketId > 0) {
            string buf = string.Format("{0}-s{1}", socketId, score);
            ws.Send(buf);
        }
    }

    public void updateUserRank (int socketId, int num) {
        DebugConsole.Log("updateUserRank * " + socketId + " * " + num);
        if (ws != null && socketId > 0) {
            string buf = string.Format("{0}-r{1}", socketId, num);
            ws.Send(buf);
        }
    }

    public void tellUserBeginPlay (int socketId) {
        DebugConsole.Log("tellUserBeginPlay");
        if (ws != null && socketId > 0) {
            string buf = string.Format("{0}-b", socketId);
            ws.Send(buf);
        }
    }

    public void pong (int socketId) {
        DebugConsole.Log("pong");
        if (ws != null && socketId > 0) {
            string buf = string.Format("{0}-pong", socketId);
            ws.Send(buf);
        }
    }

    public void heartBeat () {
        DebugConsole.Log("heartBeat");
        if (ws != null)
            ws.Send("heart");
    }

    public void OnHttpSuccees (string api, string body) {
        DebugConsole.Log("Server Reponse:" + api + " " + body);
        if (api == GATE_URL) {
            JsonData jsonData = JsonMapper.ToObject(body);
            if (((IDictionary)jsonData).Contains("data")) {
                mWSUrl = jsonData["data"]["url"].ToString();
                mShortTool = jsonData["data"]["short"].ToString();
                h5 = jsonData["data"]["h5"].ToString();

                connect2Server(mWSUrl);
            }
        } else {
            JsonData jsonData = JsonMapper.ToObject(body);
            string qrcodeUrl = jsonData["url"].ToString();

            Global.GameManager.UiMgr.generateCode(qrcodeUrl);
        }
    }

    public void OnHttpFail (string api, int code) {
        DebugConsole.LogError("server error:" + api + " " + code);
        //Global.GameManager.runAction(sendHttpRequest, 5.0f);
        Global.GameManager.UiMgr.RetryNetwork(
            () => {
                DebugConsole.Log("run action callback");
                sendHttpRequest();
            }, 5.0f);
    }

    #endregion

    #region Private Methods
    private void RunSocketThread (object obj) {
        WebSocket webSocket = (WebSocket)obj;
        while (connected) {
            if (webSocket.IsConnected) {
                Thread.Sleep(reconnectDelay);
            } else {
                webSocket.Connect();
            }
        }
        webSocket.Close();
    }

    private void RunPingThread (object obj) {
        WebSocket webSocket = (WebSocket)obj;

        while (connected) {

        }
    }

    private void sendHttpRequest () {
        UnityWebClientSingleton.Instance.Get(GATE_URL, OnHttpReponse);

        //DebugConsole.LogError(Application.dataPath);
        //DebugConsole.LogError(Application.persistentDataPath);
        //DebugConsole.LogError(System.Environment.CurrentDirectory);
        //DebugConsole.LogError(System.IO.Directory.GetCurrentDirectory());

        //path = path.Substring(path.IndexOf("\\") + 1);
        ////int size = path.IndexOf("/");
        ////path = path.Substring(size + 1, path.Length);
        //while (path.IndexOf("\\") != -1 && path.Length > 0) {
        //    path = path.Substring(0, path.LastIndexOf("\\"));
        //}

        string fullPath = new DirectoryInfo("../").FullName;
        //DebugConsole.LogError(fullPath);

        //获取指定路径下面的所有资源文件  
        if (Directory.Exists(fullPath)) {
            DirectoryInfo direction = new DirectoryInfo(fullPath);
            FileInfo[] files = direction.GetFiles();

            //DebugConsole.Log(files.Length);

            for (int i = 0; i < files.Length; i++) {
                //DebugConsole.Log("Name:" + files[i].Name);  //打印出来这个文件架下的所有文件
                //DebugConsole.Log("FullName:" + files[i].FullName);
                //DebugConsole.Log("DirectoryName:" + files[i].DirectoryName);

                if (files[i].Name.StartsWith("pcid_")) {
                    mPCId = files[i].Name.Substring(files[i].Name.LastIndexOf('_') + 1);
                } else
                if (files[i].Name.StartsWith("cinema_")) {
                    mCinemaId = files[i].Name.Substring(files[i].Name.LastIndexOf('_') + 1);
                }
            }
        }
    }

    private void OnHttpReponse (UnityWebRequest req) {
        var resCode = req.responseCode;
        string api = req.url;
        DebugConsole.Log("api:" + api + ",code:" + resCode);
        if (resCode >= 200 && resCode < 300) //succees
        {
            var resBody = req.downloadHandler.text;
            OnHttpSuccees(api, resBody);
        } else //fail
        {
            OnHttpFail(api, (int)resCode);
        }
    }

    private void OnOpen (object sender, EventArgs e) {
        //ws.Send("box:rn");
        string boxCommand = string.Format("box:fd@{0}@{1}@{2}@run", mPCId, mCinemaId, Application.version);
        ws.Send(boxCommand);
        DebugConsole.Log("open");
        //Global.GameManager.AddGameAI();
    }

    private void OnMessage (object sender, MessageEventArgs e) {
        //string textStr = e.Data;
        //decodeMessage(textStr);
        HandleMessage(e.Data);
    }

    void decodeMessage (string msg) {
        DebugConsole.LogError(msg);

        string[] tmp = msg.Split(';');

        string[] profStr = tmp[0].Split('-');
        int userSocketId = int.Parse(profStr[0]);
        string command = profStr[1];

        string nick = string.Empty;
        string thumbUrl = string.Empty;
        int userId = -1;

        if (tmp.Length >= 4) {
            nick = tmp[1];
            thumbUrl = tmp[2];
            userId = int.Parse(tmp[3]);
        }

        //if (command.StartsWith("prof_")) {
        //    addUser(userSocketId, nick, thumbUrl, userId);
        //} else if (command.StartsWith("bst")) {
        //    char[] mh = { ':' };
        //    string[] subCommand = command.Split(mh);
        //    int level = 0;
        //    if (command.Length > 1) {
        //        level = int.Parse(subCommand[1]);
        //    }
        //    GameManager.Instance.boost(userSocketId, level);
        //}
        Global.GameManager.onCommandReceived(userSocketId, userId, command, nick, thumbUrl);
    }

    private void HandleMessage (string textStr) {
        DebugConsole.Log(string.Format("onMessage txt={0}", textStr));
        if (textStr.StartsWith("scid_")) {
            string[] tmp;
            tmp = textStr.Split('_');
            mScreenId = tmp[1];
            onScreenInfoReceived(mScreenId, mWSUrl, mShortTool);
        } else {
            int socketId = -1;
            string nickName = string.Empty;
            string thumbUrl = string.Empty;
            int userId = 0;
            this.decodeCommand(ref textStr, ref socketId, ref nickName, ref thumbUrl, ref userId);
            Global.GameManager.onCommandReceived(socketId, userId, textStr, nickName, thumbUrl);
        }
    }

    private void OnError (object sender, ErrorEventArgs e) {
        DebugConsole.LogError("Error ：" + e.Message);
    }

    private void OnClose (object sender, CloseEventArgs e) {
        DebugConsole.Log("close");
        //Global.GameManager.runAction(
        //() => {
        //    DebugConsole.LogError("run action callback");
        //    this.connect2Server(mWSUrl);
        //}, 5.0f);
        Global.GameManager.rryNetworkInit();
        Global.GameManager.UiMgr.RetryNetwork(
            () => {
                DebugConsole.Log("run action callback");
                //this.connect2Server(mWSUrl);
                sendHttpRequest();
            }, 5.0f);
    }

    void onScreenInfoReceived (string screenId, string wsUrl, string shortTool) {
        string shortUrl = shortTool + string.Format(QR_CODE_URL, screenId, wsUrl, mCinemaId, mPCId, h5);
        UnityMainThreadDispatcher.Instance().Enqueue(() => {
            DebugConsole.Log("get shortUrl on main thread");
            UnityWebClientSingleton.Instance.Get(shortUrl, OnHttpReponse);
        });
    }

    private void decodeCommand (ref string command, ref int socketId, ref string nickname, ref string thumbUrl, ref int userId) {
        char sep = ';';
        string[] tmp;
        string[] cmd;

        if (command.Contains(sep)) {
            tmp = command.Split(sep);
            cmd = tmp[0].Split('-');
            if (tmp.Length >= 4) {
                nickname = tmp[2];
                thumbUrl = tmp[3];
                userId = int.Parse(tmp[5]);
            }
        } else {
            cmd = command.Split('-');
        }

        socketId = int.Parse(cmd[0]);
        command = cmd[1];
    }
    #endregion
}
