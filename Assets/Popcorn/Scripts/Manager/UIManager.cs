﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public Transform ThumbParent;
    public Transform ProgressParent;
    public TopListPanel TopListPanel;
    public GenerateQRcode mQRcode;
    public Image gameProgress;
    public Text mTipsText;

    public List<PlayerCtrl> players;

    public RectTransform topListBG;

    private static UIManager _instance;

    public static UIManager Instance {
        get {
            if (_instance == null) {
                _instance = new UIManager();
            }
            return _instance;
        }
    }

    void Awake () {

    }

    void Start () {
        TopListPanel = getRootNode().Find("GamePanel/TopListPanel").GetComponent<TopListPanel>();
        topListBG = TopListPanel.transform.Find("TopListBG").transform as RectTransform;
        ThumbParent = getRootNode().Find("GamePanel/ThumbPanel");
        ProgressParent = getRootNode().Find("GamePanel/GameProgressBar/PFList");
        gameProgress = getRootNode().Find("GamePanel/Progress/Background").GetComponent<Image>();
        gameProgress.fillAmount = 0;
    }

    private Transform rootNode = null;
    /// <summary>
    /// 获取UIRoot下的PanelWindow物体下
    /// </summary>
    /// <returns></returns>
    public Transform getRootNode () {
        if (rootNode == null) {
            rootNode = Global.PanelWindow;
        }
        return rootNode;
    }

    public void OpenTopList () {
        //TopListPanel.gameObject.SetActive(true);
        topListPlayForward();
    }

    public void CloseTopList () {
        //TopListPanel.PlayBackwards(
        //    () => TopListPanel.gameObject.SetActive(false)
        //);
        topListPlayBackwards();
    }

    public void InitProgress () {
        gameProgress.fillAmount = 0;
    }

    public void topListPlayForward () {
        TopListPanel.gameObject.SetActive(true);
        topListBG.DOLocalMove(Vector2.zero, 1);
    }

    public void topListPlayBackwards () {
        Tweener tweener = topListBG.DOLocalMove(Vector2.up * 1000, 1);
        tweener.OnComplete(
            () => TopListPanel.gameObject.SetActive(false)
        );
    }

    /// <summary>
    /// 游戏即将开始的倒计时提示
    /// </summary>
    /// <param name="callback">需要执行的回调函数</param>
    /// <param name="time">倒计时时间</param>
    public void RetryNetwork (Action callback, float time = 5f) {
        TimerManager.Register(this, 1f, () => {
            if (time > 0) {
                Global.GameManager.ActiveBlur(true);
                mTipsText.gameObject.SetActive(true);
                mTipsText.text = String.Format("网络异常，正在重连中… ({0})", time);
                time -= 1.0f;
            } else {
                mTipsText.text = "";
                mTipsText.gameObject.SetActive(false);
                Global.GameManager.ActiveBlur(false);
                callback();
                TimerManager.UnRegister(this);
            }
        });
    }

    public void generateCode (string url) {
        mQRcode.ShowQRcode(url, 666);
    }

    public void ShowDistance (Transform tf) {
        float p = (Vector3.Distance(tf.position, Global.GameManager.targetObj.transform.position) /
                   Vector3.Distance(EnvControler.PLAYER_ORIGIN_POSITION, Global.GameManager.targetObj.transform.position)) /** 100f*/;
        //p = Mathf.Clamp(p, 0, 100);

        gameProgress.fillAmount = 1 - p;
    }

    public void ClearUIList () {

    }

    // Update is called once per frame
    void Update () {

    }

    void sortProgress () {

    }
}
