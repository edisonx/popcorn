﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class GameControler : MonoBehaviour {
    private GameControler() { }
    private static GameControler _instance;
    public static GameControler Instance {
        get {
            if (null == _instance) {
                string rName = "MainTick";
                GameObject main = GameObject.Find(rName);
                if (main == null) {
                    main = new GameObject(rName);
                    main.name = rName;
                    main.tag = rName;
                }
                _instance = main.GetComponentSafe<GameControler>();
            }

            return _instance;
        }
    }

    // Use this for initialization
    void Start() {

    }

    object scriptGameInstance = null;
    Action onStartupFunc;

    public void Initialize(Action onComplete) {
        if (onComplete != null) {
            onStartupFunc = onComplete;
        }

        //取消 Destroy 对象 
        DontDestroyOnLoad(gameObject);
#if YL_DEBUG
        InitConsole();
#endif
        InitUIRoot();

        //基本设置
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = Global.FrameRate;
        UnityEngine.QualitySettings.vSyncCount = Global.VSyncCount;

        //挂载管理器并初始化
        ManagerCollect.Instance.AddManager<GameManager>(ManagerName.Game);
        ManagerCollect.Instance.AddManager<TaskManager>(ManagerName.Task);
        ManagerCollect.Instance.AddManager<SoundManager>(ManagerName.Sound);

        LoadAssetbundleManifest();
    }

#if YL_DEBUG
    void InitConsole() {
        string rName = "DebugConsloe";
        GameObject consloe = GameObject.Find(rName);
        if (consloe == null) {
            consloe = new GameObject(rName);
            consloe.name = rName;
        }
        var com = consloe.GetComponentSafe<DebugConsole>();
        com.IsDebugMode = true;
        com.isOpenBriefView = true;
    }
#endif

    void InitUIRoot() {
        string rName = "UIRoot";
        GameObject root = GameObject.Find(rName);

        if (root == null) {
            var temp = Resources.Load<GameObject>(rName);

            temp.name = rName;
            root = GameObject.Instantiate(temp);
            root.name = rName;
        }
        root.transform.Reset();
        DontDestroyOnLoad(root);
    }

    public void SetScriptGameInstance(object rObj) {
        if (rObj != null) {
            scriptGameInstance = rObj;
        }
    }

    void LoadAssetbundleManifest() {
        string bundlName = AppPlatform.GetAssetBundleDirName();
#if YL_TEST_ENV
        string host = Global.ServerConfig.GetString("Server", "debugResHost");
#else
        string host = Global.ServerConfig.GetString("Server", "resHost");
#endif

        Global.resServerURL = host;
        CheckNetwork();
    }

    void CheckNetwork() {
        if (!NetworkControler.Instance.NetWorkAvailable()) {
            StartCoroutine(RetryNetwork());
            return;
        } else {
            CheckVersion();
        }
    }

    IEnumerator RetryNetwork() {
        yield return new WaitForSeconds(5);
        CheckNetwork();
    }

    void CheckVersion() {
        DebugConsole.Log("CheckVersion !");
    }

    /// <summary>
    /// 退出游戏
    /// </summary>
    void GameEnd() {
        scriptGameInstance = null;
        UIUtil.ClearUICache();
        Util.ClearMemory();
        //Caching.ClearCache();
    }

    void OnApplicationQuit() {
        GameEnd();
    }

    // Update is called once per frame
    void Update() {
        if (scriptGameInstance != null)
            LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "Update");
    }

    void FixedUpdate() {
        if (scriptGameInstance != null)
            LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "FixedUpdate");
    }

    void LateUpdate() {
        if (scriptGameInstance != null)
            LShapUtil.CallScriptFunction(scriptGameInstance, "GameInstance", "LateUpdate");
    }
}
