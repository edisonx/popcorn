﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class TimerItem {
    /// <summary>
    /// 当前时间
    /// </summary>
    public float currentTime;

    /// <summary>
    /// 延迟时间
    /// </summary>
    public float delayTime;

    /// <summary>
    /// 回调函数
    /// </summary>
    public Action callback;

    public TimerItem(float time, float delayTime, Action callback) {
        this.currentTime = time;
        this.delayTime = delayTime;
        this.callback = callback;
    }

    /// <summary>
    /// 启动
    /// </summary>
    /// <param name="time">Time.</param>
    public void Run(float time) {
        // 计算差值
        float offsetTime = time - this.currentTime;

        // 如果差值大等于延迟时间
        if (offsetTime >= this.delayTime) {
            float count = offsetTime / this.delayTime - 1;
            float mod = offsetTime % this.delayTime;

            for (int index = 0; index < count; index++) {
                this.callback();
            }

            this.currentTime = time - mod;
        }
    }
}

/// <summary>
/// 时间管理
/// </summary>
public class TimerManager {
    /// <summary>
    /// 当前游戏运行的时间
    /// </summary>
    public static float time;

    /// <summary>
    /// 对象池
    /// </summary>
    public static Dictionary<object, TimerItem> timerList = new Dictionary<object, TimerItem>();

    /// <summary>
    /// 启动
    /// </summary>
    public static void Run() {
        // 设置时间值
        TimerManager.time = Time.time;

        TimerItem[] objectList = new TimerItem[timerList.Values.Count];
        timerList.Values.CopyTo(objectList, 0);

        // 锁定
        foreach (TimerItem timerItem in objectList) {
            if (timerItem != null) timerItem.Run(TimerManager.time);
        }
    }

    /// <summary>
    /// 注册
    /// </summary>
    /// <param name="objectItem">Object item.</param>
    /// <param name="delayTime">Delay magicTime.</param>
    /// <param name="callback">Callback.</param>
    public static void Register(object objectItem, float delayTime, Action callback) {
        if (!timerList.ContainsKey(objectItem)) {
            TimerItem timerItem = new TimerItem(TimerManager.time, delayTime, callback);
            timerList.Add(objectItem, timerItem);
        }
    }

    /// <summary>
    /// 取消注册
    /// </summary>
    /// <param name="objectItem">Object item.</param>
    public static void UnRegister(object objectItem) {
        if (timerList.ContainsKey(objectItem)) {
            timerList.Remove(objectItem);
        }
    }
}