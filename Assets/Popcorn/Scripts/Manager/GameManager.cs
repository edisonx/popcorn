﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum GameStatus {
    GAME_STATUS_IDLE,
    GAME_STATUS_STARTING,
    GAME_STATUS_GAMING,
    GAME_STATUS_OVER
}

public class StatusDelegate : MonoBehaviour {
    public virtual void onGameStarting (float after) { }

    public virtual void onGameBegin () { }

    public virtual void onGameScore (User user, int score) { }

    public virtual void onGameOver () { }

    public virtual void onUserJoin (User user) { }

    public virtual void onUserLeave (User user) { }
}

public class GameManager : StatusDelegate {
    public Dictionary<int, PlayerCtrl> mPlayingPlayers;
    public List<PlayerCtrl> mPlayerPools;

    StatusDelegate delegator;

    GameStatus mGameStatus;
    bool mLocked;

    public GameObject[] playerPrefabs;

    public RapidBlurEffect mCameraBlur;
    public GameObject targetObj;
    public UIManager UiMgr;
    public TipsPanel tipsPanel;
    public AIControler aiControler;

    public List<PlayerCtrl> mPlayingList;

    public float heartBeatTime = (3600 * 5); // 5min
    private float mFrameCounter = 0;
    public int GAME_NUM_MAX = 3;
    public int GAME_CUR_INDEX = 0;

    public float PLAYER_SPEED_MAX = 1;
    public GameObject FirstPlayer;

    public float GameRoundTime = 10f;

    public List<PlayerCtrl> PlayerRankList;

    void Awake () {
        UiMgr = GameObject.Find("UIRoot").GetComponent<UIManager>();
        NetworkControler.Instance.AwakeInit();
        //SignalListener.Instance.setDelegate(this);
        this.mGameStatus = GameStatus.GAME_STATUS_IDLE;
        //autoRestartAfterDelay(3.0f);

        mPlayingPlayers = new Dictionary<int, PlayerCtrl>();
        mPlayerPools = new List<PlayerCtrl>();
        //delegator = new StatusDelegate();
        delegator = this;

        mPlayingList = new List<PlayerCtrl>();
        PlayerRankList = new List<PlayerCtrl>();
        aiControler = GetComponent<AIControler>();
    }

    void Start () {
        NetworkControler.Instance.StartInit();
        playerPrefabs = Resources.LoadAll<GameObject>("Prefabs/Model");

        targetObj = GameObject.Find("Target");
        tipsPanel = UiMgr.getRootNode().Find("TipsPanel").GetComponent<TipsPanel>();

        //UiMgr/*UIManager.Instance*/.gameStarted(
        //    () => this.changeStatus(GameStatus.GAME_STATUS_GAMING), 6f
        //);
        Global.SoundManager.PlayBacksound("bgm", true);
        ReadyGame();
    }

    void Update () {
        // 启动计时管理器
        TimerManager.Run();

        mFrameCounter += Time.deltaTime;
        if (mFrameCounter >= heartBeatTime) {
            NetworkControler.Instance.heartBeat();
            mFrameCounter = 0;
        }
    }

    public void runAction (Action callback, float time) {
        StartCoroutine(RetryNetwork(callback, time));
    }

    IEnumerator RetryNetwork (Action callback, float time) {
        yield return new WaitForSeconds(time);
        callback();
    }

    void autoRestartAfterDelay (float seconds) {
        this.onGameStarting(seconds, false);
    }

    void changeStatus (GameStatus status) {
        DebugConsole.Log(string.Format("changeStatus({0})", status));
        mGameStatus = status;
    }

    void onGameStarting (float afterSeconds, bool mLock) {
        DebugConsole.Log(string.Format("Game Starting after {0} seconds (lock = {1})", afterSeconds, mLock));
        if (!mLocked) {
            mLocked = mLock;

            //this.delegator.onGameStarting(afterSeconds);
            this.changeStatus(GameStatus.GAME_STATUS_STARTING);
        } else {

        }
    }

    void startGame () {
        //if (mGameStatus == GameStatus.GAME_STATUS_STARTING)
        {
            this.changeStatus(GameStatus.GAME_STATUS_GAMING);
            //this.startNewRound(true);
            //AIPiloter.Instance(onUserClear(ConfigCenter.Instance.aiNumber()));
        }
    }

    //TODO 游戏重玩设置
    void startNewRound (bool restart) {
        Global.GameManager.onNewRound();
        if (restart) {
            StartGame();
        } else {

        }

        //hideAllDiffIndicator();
        //setupStage();
    }

    public bool isGaming () {
        return mGameStatus == GameStatus.GAME_STATUS_GAMING;
    }

    void onTimeUp () {
        DebugConsole.Log("=========================onTimeUp===============================");

        if (Global.GameManager.mPlayerPools.Count > 0) {
            this.gameOver();
        } else {
            this.startNewRound(false);
        }
    }

    //在介绍弹窗关闭后
    public void onIntroDialogClosed () {
        this.delegator.onGameBegin();
        this.startGame();
    }

    //在完成弹窗关闭后
    public void onFinishDialogClosed () {
        this.changeStatus(GameStatus.GAME_STATUS_IDLE);
        mLocked = false;
        float seconds = 3.0f;
        //this.autoRestartAfterDelay(seconds);
        Invoke("autoRestartAfterDelay", seconds);
        UserManager.Instance.resetTopList();
    }

    void gameOver () {
        this.delegator.onGameOver();
        this.changeStatus(GameStatus.GAME_STATUS_OVER);
        rankingResult();
        UserManager.Instance.removeAllWaitingUsers();
        Global.GameManager.removeAllPlayers();
    }

    public void onCommandReceived (int socketId, int userId, string command, string nickName, string thumbUrl) {
        DebugConsole.Log(string.Format("socketReceived socketId={0} cmd= {1}", socketId, command));
        /*
        if (command.StartsWith("mv:")) {
            PlayerCtrl bird = Global.GameManager.findPlayer(socketId);
            if (bird && bird.isPlaying()) {
                DebugConsole.Log("moving player");
                command.Remove(0, 3);
                bird.onMove(Utility.string2Vec2(command));
            }
        } else if (command.StartsWith("u")) {
            PlayerCtrl bird = Global.GameManager.findPlayer(socketId);
            if (bird && bird.isPlaying()) {
                bird.onAttack();
            }
        } */
        if (command.StartsWith("bst")) {
            PlayerCtrl player = Global.GameManager.findPlayer(socketId);
            if (player /*&& player.isPlaying()*/) {
                char[] mh = { ':' };
                string[] subCommand = command.Split(mh);
                int level = 0;
                if (command.Length > 1) {
                    level = int.Parse(subCommand[1]);
                }
                boost(socketId, level);
            }
        } else if (command.StartsWith("prof_c") || command.StartsWith("prof_r")) {
            // start with prof_c, retry with prof_r
            if (mGameStatus == GameStatus.GAME_STATUS_STARTING || mGameStatus == GameStatus.GAME_STATUS_GAMING) {
                this.createUser(socketId, userId, nickName, thumbUrl);
            } else {
                NetworkControler.Instance.kickUser(socketId);
            }
        } else if (command.StartsWith("q")) {
            UserManager.Instance.removeUser4Quit(socketId);
        } else if (command == "f") {
            if (UserManager.Instance.isPlayer(socketId)) {
                NetworkControler.Instance.replyUserIamPlaying(socketId);
            } else {
                int n = -1;
                if (UserManager.Instance.isWaiting(socketId, ref n)) {
                    NetworkControler.Instance.replyUserIamWaiting(socketId, n);
                } else {
                    DebugConsole.Log("invalid user!");
                }
            }
        } else if (command == "ping") {
            NetworkControler.Instance.pong(socketId);
        }
    }

    public void boost (int socketId, int level) {
        DebugConsole.Log("boosting");
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        Global.GameManager.pushForward(socketId, level));
    }

    private void pushForward (int socketId, int level) {
        if (mPlayingPlayers.ContainsKey(socketId)) {
            PlayerCtrl player = mPlayingPlayers[socketId];
            if (player) {
                //Rigidbody rb = player.GetComponent<Rigidbody>();
                //if (level > 0 && level <= BOOST_POWER.Length) {
                //    StartCoroutine(addBoostForce(rb, BOOST_POWER[level - 1]));
                //    player.scoring(-1, level);
                //    //StartCoroutine(playerDamage(UserManager.Instance.getUsers()[0]));
                //}
                //rb.velocity = new Vector3(0, 0, 5);
                if (player.isPlaying())
                    StartCoroutine(player.addBoostForce(level));
            }
        }
    }

    public void createUser (int socketId, int userId, string nickName, string thumbUrl) {
        DebugConsole.Log(string.Format("createUser = {0} userId={1}", socketId, userId));
        User user = new User(socketId, userId, nickName, thumbUrl);
        findPlayer4UserID(user.UserID);
        this.delegator.onUserJoin(user);
        UnityMainThreadDispatcher.Instance().Enqueue(() => {
            DebugConsole.Log("adding player on main thread");
            addPlayerMap(user);
        });
    }

    public void addPlayerMap (User user) {
        PlayerCtrl player = null;
        GameObject play = null;

        int emptyIndex = choseEmptyIndex();
        if (emptyIndex >= 0) {
            GameObject lastPlayer = Global.GameManager.getLastPlayer();

            user.setIndex(emptyIndex);

            Vector3 bornPosition;
            if (lastPlayer) {
                bornPosition = new Vector3(lastPlayer.transform.position.x, lastPlayer.transform.position.y, EnvControler.Instance.getOffset(user.getIndex()).z);
            } else {
                bornPosition = EnvControler.PLAYER_ORIGIN_POSITION;
            }

            play = GameObject.Instantiate(playerPrefabs[Random.Range(1, playerPrefabs.Length)], bornPosition, Quaternion.Euler(new Vector3(0, 90, 0))); ;
            play.name = play.name.Replace("(Clone)", "*" + user.SocketID);

            player = play.GetComponent<PlayerCtrl>();
            player.setUser(user);
            user.PlayerGameObject = play;
        } else {
            DebugConsole.Log("player full");
        }

        if (player) {
            UserManager.Instance.addUser(user);
            mPlayingPlayers.Add(user.SocketID, player);
            mPlayingList.Add(player);
        }
    }

    public void addPlayerMap (PlayerCtrl player) {
        User user = null;
        GameObject play = null;

        user = player.getUser();

        int emptyIndex = choseEmptyIndex();
        if (emptyIndex >= 0) {
            GameObject lastPlayer = Global.GameManager.getLastPlayer();

            user.setIndex(emptyIndex);

            Vector3 bornPosition;
            if (lastPlayer) {
                bornPosition = lastPlayer.transform.position + EnvControler.Instance.getOffset(user.getIndex());
            } else {
                bornPosition = EnvControler.PLAYER_ORIGIN_POSITION;
            }

            play = GameObject.Instantiate(playerPrefabs[0], bornPosition, Quaternion.Euler(new Vector3(0, 180, 0))); ;
            play.name = play.name.Replace("(Clone)", user.SocketID.ToString());

            player = play.GetComponent<PlayerCtrl>();
            player.setUser(user);
            user.PlayerGameObject = play;
        } else {
            DebugConsole.Log("player full");
        }
    }

    int choseEmptyIndex () {
        for (int i = 0; i < UserManager.Instance.PLAYER_MAX; i++) {
            bool used = false;
            List<User> mPlayers = UserManager.Instance.getUsers();
            for (int j = 0; j < mPlayers.Count; j++) {
                if (i == mPlayers[j].getIndex()) {
                    used = true;
                    break;
                }
            }
            if (!used) {
                return i;
            }
        }
        return -1;
    }

    public GameObject getLastPlayer () {
        List<User> users = UserManager.Instance.getUsers();

        if (users.Count == 0) {
            return null;
        }

        Vector3 lastPosition = users[0].PlayerGameObject.transform.position;
        int lastIndex = 0;

        for (int i = 1; i < users.Count; i++) {
            User user = users[i];
            Vector3 userPos = user.PlayerGameObject.transform.position;
            if (userPos.x < lastPosition.x) {
                lastPosition = userPos;
                lastIndex = i;
            }
        }
        return users[lastIndex].PlayerGameObject;
    }

    /// <summary>
    /// 如果没有socket id, 查找有没有idle状态的鸟, 有就复用没有, 则创建
    /// </summary>
    /// <param name="socketId"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    public PlayerCtrl newPlayer (int socketId, User user) {
        PlayerCtrl resultPlayer = null;
        foreach (int sId in mPlayingPlayers.Keys) {
            if (sId != socketId) {
                // 查找,应该没有该id才对
                if (mPlayerPools.Count == 0) {
                    // 没找到重用, 加新玩家
                    //resultPlayer = new PlayerCtrl(); //TODO 继承MonoBehaviour不可直接实例出来
                    //resultPlayer.createOne();
                    DebugConsole.Log(string.Format("newPlayer.create socketId = {0}", socketId));
                } else {
                    // 找到重用
                    resultPlayer = mPlayerPools[0];
                    mPlayerPools.Remove(mPlayerPools[0]);
                    DebugConsole.Log(string.Format("newPlayer.reuse socketId = {0}", socketId));
                }
            } else {
                // 找到
                DebugConsole.Log(string.Format("{0} the sockeit id already exists", false));
            }
        }

        if (resultPlayer) {
            resultPlayer.init();
            mPlayingPlayers[socketId] = resultPlayer;
            resultPlayer.bindUser(user);
            resultPlayer.idle();
        }

        DebugConsole.Log(string.Format("newPlayer play={0} pool={1}", mPlayingPlayers.Count, mPlayerPools.Count));
        return resultPlayer;
    }

    public PlayerCtrl findPlayer (int socketId) {
        DebugConsole.Log(string.Format("finding Player = {0}", socketId));
        PlayerCtrl resultPlayer = null;

        foreach (int sId in mPlayingPlayers.Keys) {
            if (sId == socketId) {
                resultPlayer = mPlayingPlayers[socketId];
            }
        }

        //for (int i = 0; i < mPlayerMap.Count; i++) {
        //    PlayerControler iter = mPlayerMap[i];
        //    if (mPlayerMap.ContainsKey(socketId)) {
        //        resultPlayer = iter;
        //    }
        //}

        return resultPlayer;
    }

    public void rryNetworkInit () {
        if (mPlayingList.Count > 0)
            for (int i = 0; i < mPlayingList.Count; i++) {
                findPlayer4UserID(mPlayingList[i].getUser().SocketID);
            }
    }

    public void findPlayer4UserID (int userID) {
        if (mPlayingList.Count > 0 && userID > 0)
            for (int i = 0; i < mPlayingList.Count; i++) {
                if (mPlayingList[i].getUser().UserID == userID) {
                    UserManager.Instance.removeUser4Quit(mPlayingList[i].getUser().SocketID);
                }
            }
    }

    public void releasePlayer (int socketId, bool forUser = false) {
        PlayerCtrl resultPlayer = null;
        //TODO 临时直接取值
        //foreach (int sId in mPlayingPlayers.Keys) {
        //    if (sId == socketId) {
        resultPlayer = mPlayingPlayers[socketId];
        //    }
        //    else {
        //        DebugConsole.Log(string.Format("{0} no this socket id player", socketId));
        //        return;
        //    }
        //}

        resultPlayer.unbindUser();
        mPlayerPools.Add(resultPlayer);
        if (forUser) {
            mPlayingList.Remove(resultPlayer); //TODO 为了统一清理 避免清理用户中 直接跳过
        }
        mPlayingPlayers.Remove(socketId);
        DebugConsole.Log(string.Format("releasePlayer play={0} pool={1}", mPlayingPlayers.Count, mPlayerPools.Count));
    }

    public override void onGameScore (User user, int score) {
        user.addScore(score);
        UserManager.Instance.sortPlaysWithScore();
        UserManager.Instance.sortTopList(user);
    }

    void timeEscape (float dt) {
        SignalListener.Instance.checkGameStartingSignalInMainThread();

        //TODO
        //if (mGameStatus == GameStatus.GAME_STATUS_GAMING/* || mGameStatus == GameStatus.GAME_STATUS_IDLE*/) {
        //    mTimeCounter.timeEscape(dt);
        //    BirdManager.Instance.timeEscape(dt);
        //} else if (mGameStatus == GameStatus.GAME_STATUS_STARTING) {
        //    StatusLayer.getInstance.timeEscape(dt);
        //} else if (mGameStatus == GameStatus.GAME_STATUS_OVER) {
        //    StatusLayer.Instance.timeEscape(dt);
        //    BirdManager.Instance.timeEscape(dt);
        //}
    }

    //游戏初始化
    public void onNewRound () {
        //mCamera.Init();
    }

    public void removeAllPlayers () {
        //for (int i = 0; i < mPlayingPlayers.Count; i++) {
        //    mPlayingPlayers[i].die();
        //}

        DebugConsole.Log("removeAllPlayers");

        for (int i = 0; i < mPlayingList.Count; i++) {
            User user = mPlayingList[i].getUser();
            UserManager.Instance.removeUserForGameOver(user);
        }

        mPlayingList.Clear();
    }

    public void ActiveBlur (bool isActive) {
        mCameraBlur.enabled = isActive;
    }

    public void keepTheTop3 (Action func, bool isOver = false) {
        PlayerRankList = mPlayingList;
        Global.SoundManager.PlayEffectsound("happy");
        StartCoroutine(restartGame(func, isOver));
    }

    IEnumerator restartGame (Action func, bool isOver, float time = 5f) {
        changeStatus(GameStatus.GAME_STATUS_IDLE);
        //给冲线是获取玩家的位置
        yield return new WaitForSeconds(2);
        if (func != null) func();
        if (isOver == false) {
            yield return new WaitForSeconds(time);

            List<User> winUsers = new List<User>();
            #region 取前三名加入队列
            //if (mPlayingList.Count > 0) {
            //    //踢出后三名玩家
            //    for (int i = 0; i < mPlayingList.Count; i++) {
            //        if (i < 3) {
            //            winUsers.Add(mPlayingList[i].getUser());
            //            mPlayingList[i].die(false);
            //        }
            //        else {
            //            mPlayingList[i].die(true);
            //        }
            //    }
            //    mPlayingPlayers.Clear();
            //    mPlayingList.Clear();
            //    UserManager.Instance.mPlayerQueue.Clear();
            //}
            #endregion
            if (PlayerRankList.Count > 0) {
                for (int i = 0; i < PlayerRankList.Count; i++) {
                    //TODO 排除AI
                    //if (PlayerRankList[i].getUser().SocketID >= 0) 
                    {
                        winUsers.Add(PlayerRankList[i].getUser());
                    }
                    PlayerRankList[i].die();
                }
                mPlayingPlayers.Clear();
                mPlayingList.Clear();
                PlayerRankList.Clear();
                UiMgr.ClearUIList();
                UserManager.Instance.mPlayerQueue.Clear();
            }

            yield return new WaitForSeconds(time);
            InitFunc();

            for (int i = 0; i < winUsers.Count; i++) {
                addPlayerMap(winUsers[i]);
            }

            //autoRestartAfterDelay(250);
            startNewRound(true);
        } else //新一轮开始 
        {
            yield return new WaitForSeconds(GameRoundTime);
            ReadyGame();
        }
    }

    public void InitFunc () {
        UiMgr.InitProgress();
        UiMgr.CloseTopList();
        isEndWheel = false;
    }

    //获取PlayerList中的最后一位
    public GameObject getLastPlayerGO () {
        if (mPlayingList.Count == 0) {
            return null;
        }

        Vector3 lastPosition = mPlayingList[0].transform.position;
        int lastIndex = 0;

        for (int i = 1; i < mPlayingList.Count; i++) {
            Vector3 userPos = mPlayingList[i].transform.position;
            if (userPos.x < lastPosition.x) {
                lastPosition = userPos;
                lastIndex = i;
            }
        }

        return mPlayingList[lastIndex].gameObject;
    }

    public void ShowTopListPanel () {
        UiMgr.OpenTopList();
    }

    public void AddGameAI () {
        for (int i = 0; i < aiControler.minusSocketId.Length; i++) {
            aiControler.addAIPlayer();
        }
    }

    //游戏准备开始
    public void ReadyGame () {
        if (mGameStatus == GameStatus.GAME_STATUS_IDLE) {
            changeStatus(GameStatus.GAME_STATUS_STARTING);
            StartGame();
        } else if (mGameStatus == GameStatus.GAME_STATUS_OVER)
        //新一轮开始
        {
            changeStatus(GameStatus.GAME_STATUS_STARTING);
            GAME_CUR_INDEX = 0;
            InitFunc();
            StartGame();
        }
        DebugConsole.LogError(" * TargetFrameRate:" + Application.targetFrameRate);
        this.AddGameAI();
    }

    //开始游戏
    public void StartGame () {
        StartCoroutine(tipsPanel.StartTime(
            () => changeStatus(GameStatus.GAME_STATUS_GAMING)));
        //UiMgr.gameStarted(
        //    () => changeStatus(GameStatus.GAME_STATUS_GAMING)
        //);
    }

    //暂停游戏
    void gauseGame () {
        for (int i = 0; i < mPlayingList.Count; i++) {
            mPlayingList[i].GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        }
    }

    private bool isEndWheel = false;

    //游戏重玩 每局
    public void ReplayGame () {
        if (isEndWheel)
            return;
        if (GAME_CUR_INDEX >= GAME_NUM_MAX - 1) {
            keepTheTop3(() => {
                ShowTopListPanel();
                gameOver();
            }, true);
        } else {
            keepTheTop3(() => ShowTopListPanel());
        }
        GAME_CUR_INDEX++;
    }

    //游戏结束
    public void GameOver () {
        if (isGaming()) {
            rankingResult();
            changeStatus(GameStatus.GAME_STATUS_OVER);
        }
    }

    void rankingResult () {
        for (int i = 0; i < PlayerRankList.Count; i++) {
            User user = PlayerRankList[i].getPlayerUser();
            if (user.SocketID >= 0)
                NetworkControler.Instance.updateUserRank(user.SocketID, i + 1);
            //NetworkControler.Instance.kickUser(user.SocketID);
        }
    }
}
