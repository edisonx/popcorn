﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameProgressBar : MonoBehaviour {
    public Image Progress;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void startProgress() {

    }

    public void updateProgress(float value) {
        Progress.fillAmount = value;
    }
}
