﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCtrl : MonoBehaviour {
    public Image mThumbPng;
    public Text mNickLabel;
    public GameObject mMakeProgress;
    public Image mProgressBar;

    public Animator mPlayerMakeAnim;
    public Animator mPlayerEffectAnim;
    public Animator mBombAnim;

    User mUser;

    void Awake () {

    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    private void FixedUpdate () {

    }

    public IEnumerator addBoostForce (float value) {
        yield return new WaitForSeconds(1f);
    }

    public bool isPlaying () {
        return mUser.UserState == UserState.Playing;
    }

    public void bindUser (User user) {
        DebugConsole.Log(string.Format("bindUser user(s{0} u{1} {2})", user.SocketID, user.UserID, user.NickName));
        mUser = user;

        string thumbUrl = mUser.ThumbUrl;

        if (mUser != null && string.IsNullOrEmpty(thumbUrl)) {

        }
    }

    public User getPlayerUser () {
        return mUser;
    }

    public void unbindUser () {
        DebugConsole.Log(string.Format("unbindUser user(s{0} u{1} {2})", mUser.SocketID, mUser.UserID, mUser.NickName));
        //isDeath = true;
        release();
        mUser = null;
    }

    public void createOne () {
        //TODO 打算创建屏幕显示的头像UI跟随

    }

    public void init () {
    }

    public void idle () {
    }

    public void die () {
        //TODO
        release();
    }

    public void release () {
        //TODO 作为释放
        PlayerDisable();
    }

    public void scoring (int index, int score) {
        Global.GameManager.onGameScore(mUser, score);
    }

    public void setUser (User user) {
        mUser = user;
        string thumbUrl = mUser.ThumbUrl;
        if (thumbUrl != null && thumbUrl.Length > 0) {
            //StartCoroutine(user.downloadImage(null, mThumbPng));
            //StartCoroutine(user.downloadImage(thumbUrl, () => {
            UnityMainThreadDispatcher.Instance().Enqueue(user.downloadImage(thumbUrl, () => {
                setNickName(mUser.NickName);
                setThumb(mUser.ThumbTex2D);
                setScore(mUser.Score.ToString());
            }));
        }
    }

    public User getUser () {
        if (mUser != null) {
            return mUser;
        }
        return null;
    }

    void PlayerEnable () {
        mMakeProgress.SetActive(true);
        mProgressBar.fillAmount = 0;
    }

    void PlayerDisable () {
        mNickLabel.gameObject.SetActive(false);
        mMakeProgress.SetActive(false);
    }

    public void setNickName (string nickName) {
        mNickLabel.gameObject.SetActive(true);
        mNickLabel.text = nickName;
    }

    public void setThumb (Texture2D texture) {
        mThumbPng.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }

    public void setThumb (string url) {
        StartCoroutine(LoadLocalImage(url));
    }

    IEnumerator LoadLocalImage (string url) {
        DebugConsole.Log("getting local image:" + url);
        WWW www = new WWW(url);
        yield return www;

        Texture2D texture = www.texture;
        Sprite m_sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
        mThumbPng.sprite = m_sprite;
    }

    public void setScore (string score) {
        //mScoreLabel.text = score;
    }
}
