﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleRePlay : MonoBehaviour {

	[SerializeField]
	private float rpTime;								
	private ParticleSystem[] particleSystems;			//所有例子播放器

	// Use this for initialization
	void Start () {
		particleSystems = GetComponentsInChildren<ParticleSystem> ();
		InvokeRepeating ("ParticleSystemRePlay", rpTime, rpTime);
	}

	/// <summary>
	/// Particles the system re play.
	/// 特效重复播放
	/// </summary>
	void ParticleSystemRePlay()
	{
		foreach (ParticleSystem ps in particleSystems) {
			ps.Stop ();
			ps.Play ();
		}
	}

}
