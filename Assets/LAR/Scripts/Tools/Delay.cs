using UnityEngine;
using System.Collections;

public class Delay : MonoBehaviour {
	
	public float delayTime = 1.0f;
    private bool isOnEnable = false;
    public bool delayOnStart = false;
    // Use this for initialization
    bool isStart = false;

    void Start()
    {
        isStart = true;
        if (delayOnStart) this.OnEnable();
    }
	void OnEnable () {
        if (isOnEnable == true || !isStart) return;
		gameObject.SetActive(false);
		Invoke("DelayFunc", delayTime);
        isOnEnable = true;
    }

    void OnDisable()
    {
        CancelInvoke();
    }

    void DelayFunc()
	{
        gameObject.SetActive(true);
        isOnEnable = false;
    }

}
