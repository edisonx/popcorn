/*
 * Author:		Lynn.C
 * Data:		2016-09-12 
 * Des:			工具类 用来缩放ParticleSystem
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScaleEffectParticle : MonoBehaviour {

	public class ParticleSystemData
	{
		public float startSize;
		public float startSpeed;
		public ParticleSystemShapeType shapeType;
		public float radius;
		public float boxX;
		public float boxY;
		public float boxZ;
		public Vector3 maxVelocity = new Vector3();
		public Vector3 minVelocity = new Vector3();
		public float maxLimitSpeed;
		public float minLimitSpeed;
		public Vector3 maxForce = new Vector3();
		public Vector3 minForce = new Vector3();
	}

	private float scale;
	private Dictionary<ParticleSystem, ParticleSystemData> pslist = new Dictionary<ParticleSystem, ParticleSystemData>();
	public float fScale = 1f;
	private Dictionary<Light, float> lightlist = new Dictionary<Light, float>();
	public Vector3 oldscale = Vector3.one;

	void Start()
	{
		this.oldscale = transform.localScale;
		this.RecordScale (transform);
	}

	private void RecordScale(Transform ts)
	{
		if (ts.GetComponent<Light> () != null) {
			this.lightlist.Add (ts.GetComponent<Light> (), ts.GetComponent<Light> ().range);
		}
		if (ts.GetComponent<ParticleSystem> () != null) {
			ParticleSystem ps = ts.GetComponent<ParticleSystem> ();
			ParticleSystemData psData = new ParticleSystemData ();
			ParticleSystem.ShapeModule sm;
			psData.startSize = ps.startSize;
			psData.startSpeed = ps.startSpeed;
			if (ps.shape.enabled) {
				sm = ps.shape;
				psData.radius = sm.radius;
				psData.boxX = sm.box.x;
				psData.boxY = sm.box.y;
				psData.boxZ = sm.box.z;
				psData.maxVelocity = new Vector3 (ps.velocityOverLifetime.x.constantMax, ps.velocityOverLifetime.y.constantMax, ps.velocityOverLifetime.z.constantMax);
				psData.minVelocity = new Vector3 (ps.velocityOverLifetime.x.constantMin, ps.velocityOverLifetime.y.constantMin, ps.velocityOverLifetime.z.constantMin);
				psData.maxLimitSpeed = ps.limitVelocityOverLifetime.limit.constantMax;
				psData.minLimitSpeed = ps.limitVelocityOverLifetime.limit.constantMin;
				psData.maxForce = new Vector3 (ps.forceOverLifetime.x.constantMax, ps.forceOverLifetime.y.constantMax, ps.forceOverLifetime.z.constantMax);
				psData.minForce = new Vector3 (ps.forceOverLifetime.x.constantMin, ps.forceOverLifetime.y.constantMin, ps.forceOverLifetime.z.constantMin);
			}

			this.pslist.Add (ts.GetComponent<ParticleSystem> (), psData);
		}
		foreach (Transform transform in ts) {
			this.RecordScale (transform);
		}
	}

	void Update()
	{
		this.Scale = this.fScale;
	}

	public float Scale
	{
		get
		{
			return this.scale;
		}
		set
		{
			if (value != this.scale) {
				scale = value;
				ScaleParticleSystem (value);
				ScaleLightRange (value);
			}
		}
	}

	void ScaleParticleSystem(float value)
	{
		foreach (KeyValuePair<ParticleSystem, ParticleSystemData> pair in pslist) {
			ParticleSystem ps = pair.Key;
			ParticleSystemData psData = pair.Value;
			ps.startSize = psData.startSize * value;
			ps.startSpeed = psData.startSpeed * value;
			if (ps.shape.enabled) {
				ParticleSystem.ShapeModule sm = ps.shape;
				sm.radius = psData.radius * value;
				sm.box = new Vector3 (psData.boxX * value, psData.boxY*value, psData.boxZ*value);
			}
			if (ps.velocityOverLifetime.enabled) {
				ParticleSystem.VelocityOverLifetimeModule vo = ps.velocityOverLifetime;
				ParticleSystem.MinMaxCurve mmX = new ParticleSystem.MinMaxCurve(psData.minVelocity.x * value, psData.maxVelocity.x * value);
				vo.x = mmX;
				ParticleSystem.MinMaxCurve mmY = new ParticleSystem.MinMaxCurve(psData.minVelocity.y * value, psData.maxVelocity.y * value);
				vo.y = mmY;
				ParticleSystem.MinMaxCurve mmZ = new ParticleSystem.MinMaxCurve(psData.minVelocity.z * value, psData.maxVelocity.z * value);
				vo.z = mmZ;
			}
			if (ps.limitVelocityOverLifetime.enabled) {
				ParticleSystem.LimitVelocityOverLifetimeModule lv = ps.limitVelocityOverLifetime;
				ParticleSystem.MinMaxCurve mm = new ParticleSystem.MinMaxCurve (psData.minLimitSpeed * value, psData.maxLimitSpeed * value);
				lv.limit = mm;
			}
			if (ps.forceOverLifetime.enabled) {
				ParticleSystem.ForceOverLifetimeModule fo = ps.forceOverLifetime;
				ParticleSystem.MinMaxCurve mmX = new ParticleSystem.MinMaxCurve(psData.minForce.x * value, psData.maxForce.x * value);
				fo.x = mmX;
				ParticleSystem.MinMaxCurve mmY = new ParticleSystem.MinMaxCurve(psData.minForce.y * value, psData.maxForce.y * value);
				fo.y = mmY;
				ParticleSystem.MinMaxCurve mmZ = new ParticleSystem.MinMaxCurve(psData.minForce.z * value, psData.maxForce.z * value);
				fo.z = mmZ;
			}
		}
	}

	void ScaleLightRange(float value)
	{
		foreach (KeyValuePair<Light, float> pair in lightlist) {
			pair.Key.range = pair.Value * value;
		}
	}

}
